
params <- list(
  kB=1.38e-23,
  Temp=293,
  h=1.003e-3,
  Na=6.023e23,
  rB=1e-6,
  rFe=1e-10,
  rSid=1e-9,
  DFe=1,
  DSid=0.1,
  Q=1,
  Fe0=1e-7,
  N=10000,
  SidProd=1e-9,
  SidLoss=1,
  SidMax=1e-4,
  k=1e6,
  max.uptake=500,
  D=NA
)

Diff <- function(r,params) { with(params,{ kB*Temp/(6*pi*h*r) }) }
params$DFe <- Diff(params$rFe,params)
params$DSid <- Diff(params$rSid,params)
params$DB <- Diff(params$rB,params)

D1 <- Diff(params$rFe,params)

rgb2col <- function(x,max.val=255) {
  if (any(is.na(x))) {
    return(NA)
  } else {
    return(rgb(x[1],x[2],x[3],maxColorValue=max.val))
  }
}

# Functions #

f.Fr <- function(r,k,rho,params,kappaL=6e-6) {
  k13 <- k**(1/3)
  Rk <- with(params,{ rB+k13*rFe })
  Dk <- D1/k13
  rhok <- rho/k
  surf <- 4*pi*params$rB**2
  
  Jmax <- 4*pi*Dk*Rk*rhok*k
  aB <- 16/3*with(params,rB/rFe)**2*kappaL/params$Na
  aT <- with(params,max.uptake/Na)
  
  idx <- which.min(c(Jmax,aT,aB))
  #rho0k <- with(params,{ 4*rB*kappaL/(3*rFe**2*pi*D1*Na)*k13 })
  
  if (idx == 3) {
    # dissolution limited
    message("Dissolution limited")
    cB <- aB/surf*Rk**2/Dk
    rho-cB/r
  } else if (idx == 2) {
    # transport limited
    message("Transport limited")
    cT <- aT/surf*Rk**2/Dk
    rho - cT/r
  } else {
    message("Diffusion limited")
    rho*(1-params$rB/r)
  }
}

f.Nkt <- function(t,k,rho,params) {
  .k13 <- k**(1/3)
  .Rk <- with(params,{ rB+.k13*rFe })
  .Dk <- D1/.k13
  t*rho/k*4*pi*.Rk*.Dk * (1 + 2*.Rk/sqrt(pi*.Dk*t))
}

f.phiK <- function(kk,rrho,params) {
  .k13 <- kk**(1./3.)
  .Rk <- with(params,{ rB+.k13*rFe })
  .Dk <- D1/.k13
  with(params,{ 4*pi*rrho*.Rk*.Dk })
}
f.psiK <- function(kk,kappaL) { 
  4./3. * kappaL * kk**(-1./3.) 
}

f.alphaK <- function(time,k,kL,rho,params) {
  phiK <- f.phiK(k,rho,params)
  psiK <- f.psiK(k,kL)
  phiK*(1-exp(-psiK*time))
}

f.alphaK.t <- function(crit,k,kL,rho,params) {
  phiK <- f.phiK(k,rho,params)
  psiK <- f.psiK(k,kL)
  .x <- 1.0-crit/phiK
  if (.x > 0.0) {
    -log(1.0-crit/phiK)/psiK
  } else {
    Inf
  }
}

f.Ik.t <- function(k,kL,rho,params) {
  phiK <- f.phiK(k,rho,params)/k
  psiK <- f.psiK(k,kL)
  nB <- 4*(params$rB/params$rFe)**2 * k**(-2./3.)/params$Na
  .x <- (1.0-nB*psiK/phiK)
  if (.x > 0.0 & .x <= 1.0) {
    -log(.x)/psiK
  } else {
    Inf
  }
}

f.At <- function(time,k,kappaL,rho,params,mole=TRUE) {
  phiK <- f.phiK(k,rho,params)
  psiK <- f.psiK(k,kappaL)
  crit.at <- f.alphaK.t(with(params,{ max.uptake/Na }),k,kappaL,rho,params)
  crit.It <- f.Ik.t(k,kappaL,rho,params)
  crit.t <- min(crit.at,crit.It)
  #print(c(crit.at,crit.It))
  #print(phiK*params$Na)
  if (time > crit.t & (crit.at | crit.It)) {
    At <- phiK*(crit.t+(exp(-psiK*crit.t)-1.0)/psiK)
    if (crit.at < crit.It) {
      #message("Uptake limited")
      At <- At + with(params,max.uptake/Na) * (time-crit.t)
    } else {
      #message("Dissolution limited")
      nB <- 4*(params$rB/params$rFe)**2 * k**(-2./3.)/params$Na
      At <- At + nB*k*psiK * (time-crit.It)
    }
  } else {
    At <- phiK*(time+(exp(-psiK*time)-1.0)/psiK)
  }
  if (!mole) At <- At*params$Na
  At
}

f.tau <- function(kk,kL,rho,params) {
  .f <- function(t) {
    f.At(time=t,k=kk,kappaL=kL,
         rho=rho,params=params,mole=FALSE) - 1e6
  }
  uniroot(.f,interval=c(0,1e12))$root
}

format.time <- function(s,prefix="t = ") {
  if (s < 60) return(sprintf("%s%.2g s",prefix,s))
  # convert to minutes
  s <- s / 60
  if (s < 60) return(sprintf("%s%.2g min",prefix,s))
  # convert to hours
  s <- s / 60
  if (s < 24) return(sprintf("%s%.2g h",prefix,s))
  # convert to days
  s <- s / 24
  if (s < 7) return(sprintf("%s%.2g d",prefix,s))
  # convert to weeks
  if (s < 30) return(sprintf("%s%.2g wk",prefix,s/7))
  # convert to months
  if (s/30 < 12) return(sprintf("%s%.2g mo",prefix,s/30))
  # convert to years
  return(sprintf("%s%.2g y",prefix,s/365))
}


