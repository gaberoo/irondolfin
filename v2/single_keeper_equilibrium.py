#!/usr/bin/env python3

import os
import sys
import signal
import argparse
import math
import numpy

os.environ['CC'] = 'gcc-8'
os.environ['CXX'] = 'g++-8'

##############################################################################
# Set signal handler

def signal_handler(signal, frame):
  print("Exiting...")
  sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)

##############################################################################
# FEniCS libraries

import mshr
from dolfin import *

import iron
args = iron.parser.parse_args()

##############################################################################
# Form compiler options

parameters["form_compiler"]["optimize"]     = True
parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["representation"] = "uflacs"
set_log_level(LogLevel.WARNING) # 30 = warning, 

##############################################################################
# Some parameters

zero = Constant(0.0)
aggk = Constant(args.aggk)  # iron aggregate size

Fe0  = 1e-6
fF = 1e6
gF = 1e7

Fe1 = Fe0/(1.0+gF/fF)
FeK = Fe0-Fe1

print("Boundary iron:")
print("  Fe1 = %g" % Fe1)
print("  FeK = %g" % FeK)

xmax = args.xmax
eps  = 1e-7
rad  = iron.rB

##############################################################################
# make the 1D mesh

if args.verbose > 0:
  sys.stderr.write("Generating mesh...")
mesh = IntervalMesh(args.nmesh, iron.rB, xmax)
coords = iron.expand(mesh.coordinates(), iron.rB, xmax, args.scale)
mesh.coordinates()[:] = coords
if args.verbose > 0:
  sys.stderr.write("done. Minimum distance = %g.\n" % \
                   (coords[1]-coords[0]))

#mesh = Mesh("meshes/linear.xml.gz")
#coords = mesh.coordinates()

##############################################################################
# Define the function space

element = FiniteElement("Lagrange", interval, 1)
W = FunctionSpace(mesh, element)

##############################################################################
# Define boundaries

boundaries = MeshFunction("size_t", mesh, 0)
boundaries.set_all(0)

bndry_cell = CompiledSubDomain("on_boundary && x[0] == rB", rB=iron.rB)
bndry_out  = CompiledSubDomain("on_boundary && x[0] == xmax", xmax=xmax)

bndry_cell.mark(boundaries, 1)
bndry_out.mark(boundaries, 2)

bc_out_Fk = DirichletBC(W, Constant(Fe0), bndry_out)

bcs = [ bc_out_Fk ]

##############################################################################
# Setup measure for the boundaries (needed to calculate flux)

ds = Measure('ds', domain=mesh, subdomain_data=boundaries)

##############################################################################
# Define variational problem

# Define trial and test functions
du = TrialFunction(W)
u  = Function(W)
v  = TestFunction(W)

# Define functions

# Create intial conditions and interpolate
u_init = Expression("FeK", FeK=Fe0, degree=1)
u.interpolate(u_init)

f = Constant(0.0)
DFe = Constant(iron.Diff(iron.rFe))
DFk = Expression("kB*Temp/(6.*pi*h*pow(aggk,1./3.)*rFe)",
                 element=element, kB=iron.kB, Temp=iron.Temp,
                 h=iron.h, aggk=aggk, rFe=iron.rFe)

n = FacetNormal(mesh)

if args.dim == 2:
  r2 = Expression("x[0]", degree=1)
  p = Expression("2.0*pi*x[0]", degree=1)
else:
  r2 = Expression("x[0]*x[0]", degree=1)
  p = Expression("4.0*pi*x[0]*x[0]", degree=1)

a = r2*DFk*inner(nabla_grad(u),nabla_grad(v))*dx

p = Expression("4.0*pi*x[0]*x[0]", element=element)

# Robin boundary condition for the flux
KH = Constant(1e-6)
phiMax = Constant(1e-5/4.0/math.pi)

Lu = -phiMax*u/(KH+u)*v*ds(1)

L1 = a - Lu

flux = -p*dot(nabla_grad(u),n)*ds(1)

u.interpolate(u_init)
solve(L1 == 0, u, bcs)
y = iron.dolf2nump(u, mesh, dims=1)
print(y[0])
print(assemble(flux),float(phiMax)*4*math.pi)



##############################################################################
# Create nonlinear problem and Newton solver

u.interpolate(u_init)
dL = derivative(L1, u, du)

problem = iron.SecretorsEquation(dL, L1, bcs)
solver = NewtonSolver()
solver.parameters["linear_solver"] = "lu"
solver.parameters["convergence_criterion"] = "incremental"
solver.parameters["relative_tolerance"] = 1e-10

sol = solver.solve(problem, u.vector())

total_flux = assemble(flux) * iron.Na * float(DFe)

y = iron.dolf2nump(u, mesh, dims=1)
print(y[0])
print(assemble(flux),float(phiMax)*4*math.pi*y[0])


import matplotlib as mpl
import matplotlib.pyplot as plt
mpl.rcParams['toolbar'] = 'None'

log_coords = [ math.log10(z) for z in coords ]
y = iron.dolf2nump(u, mesh, dims=2)
fig, (ax1, ax2) = plt.subplots(2, sharex=True)
ax1.set_ylim([0, 1.1e-6])
ax2.set_ylim([0, 1.1e-7])
p1, = ax1.plot(log_coords, y[:, 0], color="#4daf4a", linewidth=2.0)
p2, = ax2.plot(log_coords, y[:, 1], color="#ff7f00", linewidth=2.0)
plt.show(block=False)

input("Enter to quit.")


