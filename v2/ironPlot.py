import math
import dolfin
import iron
import numpy as np
import matplotlib.tri as tri
import matplotlib.pyplot as plt

def tri_mesh(mesh):
  n = mesh.num_vertices()
  d = mesh.geometry().dim()
  coords = mesh.coordinates()
  triangles = np.asarray([cell.entities(0) \
                          for cell in dolfin.cells(mesh)])
  mesh_coords = coords.reshape((n, d))
  triangulation = tri.Triangulation(mesh_coords[:, 0],
                                    mesh_coords[:, 1],
                                    triangles)
  return triangulation

##############################################################################

def plot_mesh(mesh, xlim=None, ylim=None, pause=False):
  triang = tri_mesh(mesh)
  fig = plt.figure(figsize=(8, 8))
  plt.triplot(triang)
  if xlim != None:
    plt.xlim([-xlim, xlim])
  if ylim != None:
    plt.ylim([-ylim, ylim])
  plt.show(block=False)
  if pause:
    tmp = raw_input('Enter to continue.')
  return fig

##############################################################################

def plot_mesh_func(mesh, func, ax=None, idx=0, xlim=None, ylim=None):
  triang = tri_mesh(mesh)
  zfaces = np.asarray([func(cell.midpoint())[idx] \
                       for cell in dolfin.cells(mesh)])
  print(min(zfaces), max(zfaces))
  if ax == None:
    fig, ax = plt.subplots()
    if xlim != None:
      ax.set_xlim([-xlim, xlim])
    if ylim != None:
      ax.set_ylim([-ylim, ylim])
  ax.tripcolor(triang, facecolors=zfaces, edgecolors='k')
  #cbar = plt.colorbar(ax=ax)
  plt.show(block=False)
  plt.pause(0.01)
  #tmp = raw_input('Enter to continue.')
  return ax

##############################################################################

def plot_mesh_func_lin(mesh, func, dist, idx=0, nsteps=100,
                       xmax=None, p=None, lims=None, prod=None,
                       Rstar=None):
  if xmax == None:
    coords = mesh.coordinates()
    xmax = max(coords[:, idx])
  c0 = dist/2.0
  c1 = math.log10(iron.rB)
  c2 = math.log10(xmax-c0)
  coords = np.arange(c1, c2, (c2-c1)/nsteps)
  vals = np.asarray([ func(dolfin.Point(10**x+c0, 0.0)) for x in coords ])
  pvals = None
  if prod != None:
    pvals = [ prod*iron.rB**2/iron.DSid/(10**r) for r in coords ]
  if Rstar != None:
    Rstar += c0
  return plot_1D(coords=coords, vals=vals, p=p, lims=lims,
                 pvals=pvals, Rstar=Rstar)
    #print vals.shape
    #tmp = raw_input('Press enter to start.')

##############################################################################

def formatTime(t):
  if (t < 60):
    return "%g s" % t
  elif (t < 60*60):
    return "%.2f min" % (t/60.0)
  elif (t < 60*60*24):
    return "%.2f h" % (t/3600.0)
  elif (t < 60*60*24*7):
    return "%.2f d" % (t/(60*60*24.0))
  elif (t < 60*60*24*30):
    return "%.2f w" % (t/(60*60*24*7.0))
  elif (t < 60*60*24*365):
    return "%.2f m" % (t/(60*60*24*30.0))
  else:
    return "%.2f a" % (t/(60*60*24*365))

##############################################################################

def plot_1D(coords, vals, p=None, new_time=None,
            lims=None, pvals=None, Rstar=None):
  if p == None:
    fig, (ax1, ax2, ax3, ax4) = plt.subplots(4, sharex=True)
    if lims != None:
      if lims[0] != None:
        ax1.set_ylim(lims[0])
      if lims[1] != None:
        ax2.set_ylim(lims[1])
      if lims[2] != None:
        ax3.set_ylim(lims[2])
      if lims[3] != None:
        ax4.set_ylim(lims[3])
        #ax4.set_ylim((0, 4*(iron.rB/iron.rFe)**2))
    if Rstar != None:
      ax1.axvline(math.log10(Rstar), color="grey")
      ax2.axvline(math.log10(Rstar), color="grey")
      ax3.axvline(math.log10(Rstar), color="grey")
      ax4.axvline(math.log10(Rstar), color="grey")
    if pvals != None :
      ax2.plot(coords, pvals, color="black", linewidth=1)
    p1, = ax1.plot(coords, vals[:, 0], color="#4daf4a", linewidth=2)
    p2, = ax2.plot(coords, vals[:, 1], color="#ff7f00", linewidth=2)
    p3, = ax3.plot(coords, vals[:, 2], color="#984ea3", linewidth=2)
    p4, = ax4.plot(coords, vals[:, 3], color="#333333", linewidth=2)
    time = ax1.text(0, lims[0][1]*1.1, "t = 0")
    plt.show(block=False)
    plt.pause(0.01)
    return (p1, p2, p3, p4, time)
  else:
    (p1, p2, p3, p4, time) = p
    p1.set_data(coords, vals[:, 0])
    p2.set_data(coords, vals[:, 1])
    p3.set_data(coords, vals[:, 2])
    p4.set_data(coords, vals[:, 3])
    time.set_text(formatTime(new_time))
    plt.draw()
    plt.pause(0.01)


