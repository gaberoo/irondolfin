#!/usr/bin/env python3

import os
import sys
import signal
import argparse
import math
import numpy

os.environ['CC'] = 'gcc-8'
os.environ['CXX'] = 'g++-8'

##############################################################################
# Set signal handler

def signal_handler(signal, frame):
  print("Exiting...")
  sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)

##############################################################################
# FEniCS libraries

import mshr
from dolfin import *

import iron
args = iron.parser.parse_args()

##############################################################################
# Numpy/Matplotlib libraries

if args.plot:
  import ironPlot
  import matplotlib as mpl
  import matplotlib.pyplot as plt
  mpl.rcParams['toolbar'] = 'None'

##############################################################################
# Form compiler options

parameters["form_compiler"]["optimize"]     = True
parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["representation"] = "uflacs"
set_log_level(LogLevel.WARNING) # 30 = warning, 

##############################################################################
# Some parameters

zero = Constant(0.0)

fdt  = args.dt
T    = args.maxTime         # total simulation time
aggk = Constant(args.aggk)  # iron aggregate size
Fe0  = 1e-6

fF = 1e1
gF = 1e2

Fe1 = Fe0/(1.0+gF/fF)
FeK = Fe0-Fe1

print("Boundary iron:")
print("  Fe1 = %g" % Fe1)
print("  FeK = %g" % FeK)

if fdt < 0.0:
  sys.stderr.write("Increasing dt selected.\n")
  dt = Expression("dt", dt=-fdt, degree=1)
else:
  dt = Expression("dt", dt=fdt, degree=1)

xmax = args.xmax
eps  = 1e-7
rad  = iron.rB

##############################################################################
# make the 1D mesh

if args.verbose > 0:
  sys.stderr.write("Generating mesh...")
mesh = IntervalMesh(args.nmesh, iron.rB, xmax)
coords = iron.expand(mesh.coordinates(), iron.rB, xmax, args.scale)
mesh.coordinates()[:] = coords
if args.verbose > 0:
  sys.stderr.write("done. Minimum distance = %g.\n" % \
                   (coords[1]-coords[0]))

#mesh = Mesh("meshes/linear.xml.gz")
#coords = mesh.coordinates()

##############################################################################
# Define the function space

element = FiniteElement("Lagrange", interval, 1)
W = VectorFunctionSpace(mesh, element, 1, 2)

##############################################################################
# Define boundaries

boundaries = MeshFunction("size_t", mesh, 0)
boundaries.set_all(0)

bndry_cell = CompiledSubDomain("on_boundary && x[0] == rB", rB=iron.rB)
bndry_out  = CompiledSubDomain("on_boundary && x[0] == xmax", xmax=xmax)

bndry_cell.mark(boundaries, 1)
bndry_out.mark(boundaries, 2)

bc_out_Fk = DirichletBC(W.sub(0), Constant(FeK), bndry_out)
bc_out_F  = DirichletBC(W.sub(1), Constant(Fe1), bndry_out)
bc_cell_F = DirichletBC(W.sub(1), zero, bndry_cell)

bcs = [ bc_out_Fk, bc_out_F, bc_cell_F ]

##############################################################################
# Setup measure for the boundaries (needed to calculate flux)

ds = Measure('ds', domain=mesh, subdomain_data=boundaries)

##############################################################################
# Define variational problem

# Define trial and test functions
du = TrialFunction(W)
u  = Function(W)
u0 = Function(W)
vFk, vF = TestFunctions(W)

# Define functions

# Split mixed functions
Fk,  F  = split(u)
Fk0, F0 = split(u0)

# Create intial conditions and interpolate
u_init = Expression(("FeK","Fe1"), FeK=FeK, Fe1=Fe1, degree=1)
u.interpolate(u_init)
u0.interpolate(u_init)

f = Constant(0.0)
DFe = Constant(iron.Diff(iron.rFe))
DFk = Expression("kB*Temp/(6.*pi*h*pow(aggk,1./3.)*rFe)",
                 element=element, kB=iron.kB, Temp=iron.Temp,
                 h=iron.h, aggk=aggk, rFe=iron.rFe)

n = FacetNormal(mesh)

if args.dim == 2:
  r2 = Expression("x[0]", degree=1)
  p = Expression("2.0*pi*x[0]", degree=1)
else:
  r2 = Expression("x[0]*x[0]", degree=1)
  p = Expression("4.0*pi*x[0]*x[0]", degree=1)

aFk = dt*r2*DFk*inner(nabla_grad(Fk),nabla_grad(vFk))*dx
aF  = dt*r2*DFe*inner(nabla_grad(F),nabla_grad(vF))*dx
a0 = aFk + aF

f = Constant(fF)*Fk
g = Constant(gF)*F

L0 = - dt*r2*(f-g)*vFk*dx - dt*r2*(g-f)*vF*dx

U1 = r2*F*vF*dx + r2*Fk*vFk*dx
U0 = r2*F0*vF*dx + r2*Fk0*vFk*dx

L = (U1-U0) + (a0-L0)
dL = derivative(L, u, du)

p = Expression("4.0*pi*x[0]*x[0]", element=element)
flux = -p*dot(nabla_grad(F),n)*ds(1)

##############################################################################
# Create nonlinear problem and Newton solver

problem = iron.SecretorsEquation(dL, L, bcs)
solver = NewtonSolver()
solver.parameters["linear_solver"] = "lu"
solver.parameters["convergence_criterion"] = "incremental"
solver.parameters["relative_tolerance"] = 1e-10

t = 0.0
total_iron = 0.0

if args.output != None:
  numpy.savetxt("%s_coords.txt" % args.output, coords[:, 0])
  Fk_out = open("%s_Fk.txt" % args.output, "w")
  F_out = open("%s_F.txt" % args.output, "w")
  time_out = open("%s_time.txt" % args.output, "w")
  iron_out = open("%s_iron.txt" % args.output, "w")

t = 0.0
next_print = args.reportTime
total_iron = 0.0

if args.plot:
  log_coords = [ math.log10(z) for z in coords ]
  y = iron.dolf2nump(u, mesh, dims=2)
  fig, (ax1, ax2) = plt.subplots(2, sharex=True)
  ax1.set_ylim([0, 1.1e-6])
  ax2.set_ylim([0, 1.1e-7])
  p1, = ax1.plot(log_coords, y[:, 0], color="#4daf4a", linewidth=2.0)
  p2, = ax2.plot(log_coords, y[:, 1], color="#ff7f00", linewidth=2.0)
  plt.show(block=False)
  plt.pause(0.01)

while (t < T):
  t += abs(fdt)

  u0.assign(u)
  sol = solver.solve(problem, u.vector())

  total_flux = assemble(flux) * iron.Na*float(DFe)
  total_iron += abs(fdt)*float(total_flux)

  if fdt < 0.0:
    fdt = fdt * 1.2
    if abs(fdt) > args.maxdt:
      fdt = args.maxdt
      dt.dt = fdt
    else:
      dt.dt = -fdt

  if args.verbose > 1:
    sys.stderr.write("New dt = %g\n" % fdt)

  if args.plot or args.output != None:
    y = iron.dolf2nump(u, mesh, dims=2)

  if args.plot:
    p1.set_data(log_coords, y[:, 0])
    p2.set_data(log_coords, y[:, 1])
    plt.draw()
    plt.pause(0.01)

  if args.output != None:
    iron.write(Fk_out, y[:, 0], time=None)
    iron.write(F_out, y[:, 1], time=None)
    time_out.write("%g\n" % t)
    iron_out.write("%g %g\n" % (total_flux, total_iron))

  if args.verbose > 0:
    sys.stderr.write("Flux (t=%12f) = %g\r" % (t, total_flux))

  if t >= next_print:
    next_print += args.reportTime
    print("%g %g %g %g %g %g" % (
          t, total_flux, total_iron, args.aggk, 
          assemble(flux)*float(DFe), 
          iron.KeeperFlux(float(aggk),Fe1)))

if args.output != None:
  F_out.close()
  Fk_out.close()
  time_out.close()
  iron_out.close()

