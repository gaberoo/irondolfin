#!/bin/bash

export FENICS_CONF_SILENT=1
#source /Applications/FEniCS.app/Contents/Resources/share/fenics/fenics.conf

./rect_mesh.py --size 10 --output $1 --refine 4 \
               --xmax $3 --ymax $3 --distance $2 --factor 1.0
