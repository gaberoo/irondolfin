#!/usr/bin/env python3

import argparse
import dolfin
import mshr
import iron

parser = argparse.ArgumentParser(description='Make linear mesh.')
parser.add_argument('--nmesh', metavar='points', type=int, default=100,
                    help="Number of mesh points")
parser.add_argument('--xmax', metavar='pos', type=float, default=0.1,
                    help='Maximum radial distance')
parser.add_argument('--scale', metavar='value', type=float, default=4.0,
                    help="Mesh stretching parameter")
parser.add_argument('--output', metavar='filename', default="mesh.xml",
                    help="Output file")

args = parser.parse_args()

mesh = dolfin.IntervalMesh(args.nmesh, iron.rB, args.xmax)

coords = iron.expand(mesh.coordinates(), iron.rB, args.xmax, args.scale)

mesh.coordinates()[:] = coords

dolfin.File(args.output) << mesh


