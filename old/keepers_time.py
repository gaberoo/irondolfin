#!/Applications/FEniCS.app/Contents/Resources/bin/python

from mshr import *
from dolfin import *

##############################################################################
# Command line arguments

import argparse
parser = argparse.ArgumentParser(description='Iron diffusion with two cells.')
parser.add_argument('distance', metavar='d', type=float,
                   help='distance between the two cells (centers)')
parser.add_argument('--mesh', metavar='meshfile', help="Mesh filename")
parser.add_argument('--aggk', metavar='k', type=int, default=1,
                    help='Aggregation')
parser.add_argument("--output", metavar="file", default=None,
                    help="Output filenames")
parser.add_argument("--verbose", metavar="level", type=int, default=40,
                    help="Logging level")
parser.add_argument("--domain", metavar="size", type=float, default=1e-4,
                    help="Domain size")
args = parser.parse_args()

##############################################################################
# Some parameters

from iron import *

T    = 1e-3                    # total simulation time
dt   = Constant(1e-5)          # time step
aggk = args.aggk               # iron aggregate size
DFe  = Diff(aggk**(1./3.)*rFe) # diffusion coefficient of iron
Fe0  = 1e-7

print "Diffusion coefficient = %g" % DFe

distance  = args.distance      # distance between cells
mesh_file = None               # mesh file (must correspond to distance)

xmax = args.domain 
ymax = args.domain
eps  = 1e-7
rad  = rB

plt = None
plt1 = None

##############################################################################
# make the 2D mesh

mesh = None

p1 = Point(-distance/2.0,0.0)
p2 = Point( distance/2.0,0.0)

if mesh_file == None:
    mesh = make_mesh(distance,xmax,ymax,20,3,rad,True)
else:
    mesh = Mesh(mesh_file)

#if plt == None:
#    plt = plot(mesh)
#else:
#    plt.plot(mesh)

##############################################################################
# Define the mesh and function space

V = FunctionSpace(mesh, "Lagrange", 1)

##############################################################################
# Define boundary conditions

Fe = Constant(Fe0)
zero = Constant(0.0)

def inner_boundary(x):
    r1 = sqrt((x[0]-p1[0])**2+(x[1]-p1[1])**2)
    r2 = sqrt((x[0]-p2[0])**2+(x[1]-p2[1])**2)
    return abs(r1-rad) < eps or abs(r2-rad) < eps

class CellBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and inner_boundary(x)

class DomainBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and not inner_boundary(x)

bndry_cell = CellBoundary()
bndry_out = DomainBoundary()

bc_out  = DirichletBC(V, Fe, bndry_out)
bc_cell = DirichletBC(V, zero, bndry_cell)

boundaries = FacetFunction("size_t", mesh)
boundaries.set_all(0)
bndry_cell.mark(boundaries,1)
bndry_out.mark(boundaries,2)

bcs = [ bc_out, bc_cell ]

##############################################################################
# Setup measure for the boundaries (needed to calculate flux)

ds = Measure('ds', domain=mesh, subdomain_data=boundaries)

##############################################################################
# Define variational problem

x = SpatialCoordinate(mesh)

u = TrialFunction(V)
v = TestFunction(V)

f = Constant(0.0)
r = Expression("fabs(x[1])")
a0 = r*DFe*inner(nabla_grad(u),nabla_grad(v)) * dx
L0 = r*f*v*dx # f = 0.0

##############################################################################
# Find the steady state

uF = Function(V)
problem = LinearVariationalProblem(a0, L0, uF, bcs)
solver = LinearVariationalSolver(problem)
solver.solve()

plt1 = plot(uF,mode="color",title="Steady-state solution")
variable = raw_input('Enter to continue.')

##############################################################################
# Get the time depentent solution

u0 = project(Fe,V)
plt1.plot(u0)

u1 = Function(V)

#a1 = u*v*dx + dt*a0
#a1 =  a1_M + dt*inner(r*nabla_grad(u),r*nabla_grad(v)) * dx
a1 = r*u*v*dx
L1 = r*u0*v*dx

#M = assemble(a1)
#K = assemble(a0)
#A = M + float(dt)*K

t = float(dt)

F = interpolate(f,V).vector()

# Total flux
n = FacetNormal(mesh)
p = Expression("pi*fabs(x[1])")
flux = -p*dot(nabla_grad(u1),n)*ds(1)

#print total_flux*Na*DFe

while t <= T:
    a = a1 + dt*a0
    L = L1 + dt*L0
    solve(a == L, u1, bcs)

    #b = M*u0.vector() + float(dt)*M*F
    # apply boundary conditions
    #bc_out.apply(A,b)
    #bc_cell.apply(A,b)
    #solve(A, u1.vector(), b)

    total_flux = assemble(flux) * 0.5 * Na*DFe
    print "Time = %g, flux = %g" % (t, total_flux)

    u0.assign(u1)
    t += float(dt)

    plt1.plot(u1)

