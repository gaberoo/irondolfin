#!/Applications/FEniCS.app/Contents/Resources/bin/python

from mshr import *
from dolfin import *
import numpy

# make the 2D mesh

p1 = Point(-4.,0.)
p2 = Point( 4.,0.)

xmax = 10.0
ymax = 5.0
r = 0.5

left  = Point(-xmax,-ymax)
right = Point( xmax, ymax)
rect = Rectangle(left,right)

C1 = Circle(p1,r)
C2 = Circle(p2,r)

domain = rect - C1 - C2
mesh = generate_mesh(domain,20)

# setup the Poisson equation

V = FunctionSpace(mesh, 'Lagrange', 1)

# Define boundary conditions
#u0 = Expression("1 + x[0]*x[0] + 2*x[1]*x[1]")
u0 = Constant(100.0)
zero = Constant(0.0)

def r2(x):
    return x[0]*x[0] + x[1]*x[1]

def in_circle(C,x):
    p = C.center()
    r = C.radius()
    y0 = x[0]-p[0]
    y1 = x[1]-p[1]
    return y0*y0+y1*y1 <= r*r

def outer_boundary(x):
    return abs(x[0]) >= xmax or abs(x[1]) >= ymax

class Bin(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and not outer_boundary(x)

class Bout(SubDomain):  # define the Dirichlet boundary
    def inside(self, x, on_boundary):
        return on_boundary and outer_boundary(x)

b_out = Bout()
b_in  = Bin()
bc_out = DirichletBC(V, u0,   b_out)
bc_in = DirichletBC(V, zero, b_in)

u1 = interpolate(u0, V)

T = 10.0        # total simulation time
dt = 0.01      # time step

# Laplace term
u = TrialFunction(V)
v = TestFunction(V)
r = Expression("x[1]")
a_K = inner(r*nabla_grad(u),r*nabla_grad(v))*dx

# "Mass matrix" term
a_M = u*v*dx

M = assemble(a_M)
K = assemble(a_K)
A = M + dt*K

f = Constant(0.0)

# Compute solution
u = Function(V)
t = dt

# Plot solution and mesh
#plot(u)
#plot(mesh)

while t <= T:
    print 'time =', t
    # f.t = t
    f_k = interpolate(f, V)
    F_k = f_k.vector()
    b = M*u1.vector() + dt*M*F_k
    bc_out.apply(A, b)
    bc_in.apply(A, b)
    solve(A, u.vector(), b)

    # Verify
    u_e = interpolate(u0, V)
    u_e_array = u_e.vector().array()
    u_array = u.vector().array()
    #print 'Max error, t=%-10.3f:' % t, numpy.abs(u_e_array - u_array).max()

    t += dt
    u1.assign(u)

    plt = plot(u)
    #plt.write_png("plt/2d_%.2f" % t)
