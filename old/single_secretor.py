#!/usr/bin/env python3

import sys
import signal
import argparse

##############################################################################
# Set signal handler

def signal_handler(signal, frame):
    print("Exiting...")
    sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)

##############################################################################
# Numpy/Matplotlib libraries

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt  
mpl.rcParams['toolbar'] = 'None'

##############################################################################
# FEniCS libraries

import mshr
from dolfin import (inner, nabla_grad, dx,
                    Constant, Expression, DirichletBC, Measure,
                    MeshFunction, FiniteElement, FunctionSpace,
                    Function, TestFunctions, TrialFunction)

from iron import *

parser = argparse.ArgumentParser(description='Iron diffusion with two cells.')
parser.add_argument('--nmesh', metavar='points', type=int, default=100,
                    help="Number of mesh points")
parser.add_argument('--scale', metavar='value', type=float, default=4.0,
                    help="Mesh stretching parameter")
parser.add_argument('--aggk', metavar='k', type=float, default=1.0,
                    help='Aggregation')
parser.add_argument('--dt', metavar='time', type=float, default=1.0,
                    help='Time step')
parser.add_argument('--maxdt', metavar='time', type=float, default=1000.0,
                    help='Max time step')
parser.add_argument('--reportTime', metavar='time', type=float, 
                    default=float("inf"), help='Reporting time step')
parser.add_argument('--maxTime', metavar='time', type=float, 
                    default=1000.0, help='Max time')
parser.add_argument('--prod', metavar='rate', type=float, default=SidProd,
                    help='Siderophore production rate')
parser.add_argument('--xmax', metavar='pos', type=float, default=0.1,
                    help='Maximum radial distance')
parser.add_argument('--xview', metavar='len', type=float, default=None,
                    help='Viewport size')
parser.add_argument("--output", metavar="file", default=None,
                    help="Output filename")
parser.add_argument("--verbose", metavar="level", type=int, default=0,
                    help="Logging level")
parser.add_argument("--plot", action="store_true")
parser.add_argument("--dim", metavar='n', type=int, default=3,
                    help="dimensions")
args = parser.parse_args()

##############################################################################
# Form compiler options

parameters["form_compiler"]["optimize"]     = True
parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["representation"] = "quadrature"
set_log_level(WARNING)

##############################################################################
# Some parameters

fdt  = args.dt
T    = args.maxTime            # total simulation time
aggk = Constant(args.aggk)     # iron aggregate size
Fe0  = 1e-7

Rstar, fDFe = R_star(args.prod,aggk,Fe0)

if fdt < 0.0:
    sys.stderr.write("Increasing dt selected.\n")
    dt = Expression("dt",dt=-fdt)
else:
    dt = Expression("dt",dt=fdt)

xmax = args.xmax
eps  = 1e-7
rad  = rB

##############################################################################
# make the 1D mesh

if args.verbose > 0:
    sys.stderr.write("Generating mesh...")
mesh = IntervalMesh(args.nmesh,rB,xmax)
coords = expand(mesh.coordinates(),rB,xmax,args.scale)
mesh.coordinates()[:] = coords
if args.verbose > 0:
    sys.stderr.write("done. Minimum distance = %g.\n" % (coords[1]-coords[0]))

##############################################################################
# Define the function space

V = FunctionSpace(mesh, "CG", 1)  # CG = Lagrange
W = MixedFunctionSpace([V,V,V])

##############################################################################
# Define boundaries

Fe = Constant(Fe0)
zero = Constant(0.0)

class CellBoundary1D(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and x < rB*1.5

class DomainBoundary1D(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and x > xmax*.9

boundaries = FacetFunction("size_t", mesh)
boundaries.set_all(0)

bndry_out  = DomainBoundary1D()
bndry_cell = CellBoundary1D()

bndry_cell.mark(boundaries,1)
bndry_out.mark(boundaries,2)

bc_out_F  = DirichletBC(W.sub(0), Fe,    bndry_out)
bc_out_X  = DirichletBC(W.sub(1), zero,  bndry_out)
bc_out_Y  = DirichletBC(W.sub(2), zero,  bndry_out)
bc_cell_Y = DirichletBC(W.sub(2), zero,  bndry_cell)

bcs = [ bc_out_F, bc_out_X, bc_out_Y, bc_cell_Y ]

##############################################################################
# Setup measure for the boundaries (needed to calculate flux)

ds = Measure('ds', domain=mesh, subdomain_data=boundaries)

##############################################################################
# Define variational problem

# Define trial and test functions
du = TrialFunction(W)
u  = Function(W)
u0 = Function(W)
vF, vX, vY = TestFunctions(W)

# Define functions

# Split mixed functions
F,  X,  Y  = split(u)
F0, X0, Y0 = split(u0)

f = Constant(0.0)
kappa = Constant(1e6)

gX = Constant(args.prod)
DFe = Expression("kB*Temp/(6.*pi*h*pow(aggk,1./3.)*rFe)",
                 kB=kB,Temp=Temp,h=h,aggk=aggk,rFe=rFe)
fXY = kappa*X*F

n = FacetNormal(mesh)

if args.dim == 2:
    r2 = Expression("x[0]")
    p = Expression("2.0*pi*x[0]")
else:
    r2 = Expression("x[0]*x[0]")
    p = Expression("4.0*pi*x[0]*x[0]")

aF = dt*r2*DFe *inner(nabla_grad(F),nabla_grad(vF)) * dx
aX = dt*r2*DSid*inner(nabla_grad(X),nabla_grad(vX)) * dx
aY = dt*r2*DSid*inner(nabla_grad(Y),nabla_grad(vY)) * dx 
a0 = aF + aX + aY

Lg = dt*r2*gX*vX*ds(1)
L0 = - dt*r2*fXY*vF*dx - dt*r2*fXY*vX*dx + dt*r2*fXY*vY*dx 

U1 = r2* F*vF*dx + r2* X*vX*dx + r2* Y*vY*dx
U0 = r2*F0*vF*dx + r2*X0*vX*dx + r2*Y0*vY*dx

flux = -p*dot(nabla_grad(Y),n)*ds(1)

L = (U1-U0) + (a0-L0-Lg)
dL = derivative(L, u, du)

##############################################################################
# Create nonlinear problem and Newton solver

problem = SecretorsEquation(dL,L,bcs)
solver = NewtonSolver()
solver.parameters["linear_solver"] = "lu"
solver.parameters["convergence_criterion"] = "incremental"
solver.parameters["relative_tolerance"] = 1e-10

# Create intial conditions and interpolate
u_init = InitialConditions(Fe0)
u.interpolate(u_init)
u0.interpolate(u_init)

if args.output != None:
    u_out = File(args.output)

if args.plot:
    log_coords = [ math.log10(z) for z in coords ]
    y = dolf2nump(u,mesh)
    lims = [ (0,1e-7), (0,args.prod*rB**2/DSid/rB), (0,args.prod**.85) ]
    pvals = [ args.prod*rB**2/DSid/r for r in coords ]
    (p1, p2, p3) = plot_1D(log_coords,y,lims=lims,pvals=pvals,Rstar=Rstar)
    tmp = raw_input('Press enter to start.')

t = 0.0
next_print = args.reportTime
total_iron = 0.0

while (t < T):
    t += abs(fdt)
    u0.assign(u)
    solver.solve(problem, u.vector())
    total_flux = assemble(flux) * Na*DSid
    total_iron += abs(fdt)*float(total_flux)
    if fdt < 0.0:
        fdt = fdt * 1.2
        if abs(fdt) > args.maxdt:
            fdt = args.maxdt
            dt.dt = fdt
        else:
            dt.dt = -fdt
        if args.verbose > 1:
            sys.stderr.write("New dt = %g\n" % fdt)
    if args.output != None:
        u_out << u
    if args.plot:
        y = dolf2nump(u,mesh)
        plot_1D(log_coords,y,(p1,p2,p3))
    if args.verbose > 0:
        sys.stderr.write("Flux (t=%12f) = %g\r" % (t,total_flux))
    if t >= next_print:
        next_print += args.reportTime
        print("%g %g %g %g %g" % 
              (t, total_flux, total_iron, args.aggk, args.prod))

if args.plot:
    tmp = raw_input('Enter to quit.')

