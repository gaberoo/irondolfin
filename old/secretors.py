#!/usr/bin/env python2.7

import mshr
import dolfin

##############################################################################
# Form compiler options

dolfin.parameters["form_compiler"]["optimize"]     = True
dolfin.parameters["form_compiler"]["cpp_optimize"] = True
dolfin.parameters["form_compiler"]["representation"] = "quadrature"

##############################################################################
# Class representing the intial conditions

class InitialConditions(dolfin.Expression):
  def __init__(self,Fe):
    self.Fe = Fe
  def eval(self, values, x):
    values[0] = self.Fe
    values[1] = 1e-6
    values[2] = 0.0
  def value_shape(self):
    return (3,)

##############################################################################
# Class for interfacing with the Newton solver

class SecretorsEquation(dolfin.NonlinearProblem):
  def __init__(self, a, L):
    dolfin.NonlinearProblem.__init__(self)
    self.L = L
    self.a = L
  def F(self, b, x):
    dolfin.assemble(self.L, tensor=b)
  def J(self, A, x):
    dolfin.assemble(self.a, tensor=A)

##############################################################################
# Some parameters

import iron

T    = 1e-2                    # total simulation time
dt   = dolfin.Constant(1e-4)          # time step
aggk = 10000                   # iron aggregate size
DFe  = iron.Diff(aggk**(1./3.)*iron.rFe) # diffusion coefficient of iron
Fe0  = 1e-7

print "Diffusion coefficient = %g" % DFe

distance  = 4e-6               # distance between cells
mesh_file = None               # mesh file (must correspond to distance)

xmax = 1e-5
ymax = 5e-6
eps  = 1e-7
rad  = iron.rB

plt = None
plt1 = None

##############################################################################
# make the 2D mesh

mesh = None

p1 = dolfin.Point(-distance/2.0,0.0)
p2 = dolfin.Point( distance/2.0,0.0)
cells = [p1,p2]

if mesh_file == None:
  mesh = iron.make_mesh(distance,xmax,ymax,10,1,rad,True,2.0)
else:
  mesh = dolfin.Mesh(mesh_file)

#if plt == None:
#    plt = plot(mesh)
#else:
#    plt.plot(mesh)

boundaries = dolfin.FacetFunction("size_t", mesh)
boundaries.set_all(0)
bndry_cell = iron.CellBoundary(cells,rad,eps)
bndry_out  = iron.DomainBoundary(cells,rad,eps)
bndry_cell.mark(boundaries,1)
bndry_out.mark(boundaries,2)

##############################################################################
# Define the function space

V = FunctionSpace(mesh, "CG", 1)  # CG = Lagrange
W = MixedFunctionSpace([V,V,V])

##############################################################################
# Define boundaries

Fe = Constant(Fe0)
XProd = Constant(1e-6)
zero = Constant(0.0)

bc_out_F  = DirichletBC(W.sub(0), Fe,    bndry_out)
#bc_cell_F = DirichletBC(W.sub(0), zero,  bndry_cell)

bc_out_X  = DirichletBC(W.sub(1), zero,  bndry_out)
#bc_cell_X = DirichletBC(W.sub(1), XProd, bndry_cell)

bc_out_Y  = DirichletBC(W.sub(2), zero,  bndry_out)
bc_cell_Y = DirichletBC(W.sub(2), zero,  bndry_cell)

bcs = [ bc_out_F, bc_out_X, bc_out_Y, bc_cell_Y ]

##############################################################################
# Setup measure for the boundaries (needed to calculate flux)

ds = Measure('ds', domain=mesh, subdomain_data=boundaries)

##############################################################################
# Define variational problem

x = SpatialCoordinate(mesh)

# Define trial and test functions
du         = TestFunction(W)
u          = Function(W)
vF, vX, vY = TestFunctions(W)

# Define functions

# Split mixed functions
F, X, Y = split(u)

# Create intial conditions and interpolate
u_init = InitialConditions(Fe0)
u.interpolate(u_init)

f = Constant(0.0)
kappa = Constant(1e6)

fXY = kappa*X*F
r = Expression("fabs(x[1])")
gX = Constant(SidProd)

#a0 = r*DFe*inner(nabla_grad(u),nabla_grad(v)) * dx
#L0 = 0.0 
a0 =   r*DFe *inner(nabla_grad(F),nabla_grad(vF))*dx \
     + r*DSid*inner(nabla_grad(X),nabla_grad(vX))*dx \
     + r*DSid*inner(nabla_grad(Y),nabla_grad(vY))*dx
L0 = -r*fXY*vF*dx - r*fXY*vX*dx + r*fXY*vY*dx + r*gX*vX*ds(1)

##############################################################################
# Find the steady state

L = a0 - L0

print "Solving using linear solver..."
solve(L == 0.0, u, bcs)
#      solver_parameters = {"newton_solver": { "relative_tolerance": 1e-6 }})

plot(F)
plot(X)
plot(Y)

#file = File("secretor.pvd","compressed")
#file << u.split()[0]

tmp = raw_input('Enter to continue.')

