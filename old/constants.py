import math

# Some constants
kB = 1.38e-23                     # Boltzman's constant
Temp = 293                        # Temperature
h = 1.003e-3                      # Viscosity
Na = 6.023e23                     # Avogadro's number
rB = 1e-6                         # Bacterial radius [m]
rFe = 1e-10                       # Iron iron radius [m]
rSid = 1e-9                       # Siderophores radius [m]
Fe0 = 1e-7                        # Background concentration of iron
SidLoss = 1                       # Siderophore loss rate
SidMax = 1e-4
kappa = 1e6                       # Siderophore-Iron binding affinity
max_uptake = 500                  # Maximum uptake rate

def Diff(r):
    return kB*Temp/(6.0*math.pi*h*r)

DSid = Diff(rSid)                 # diffusion coefficient of siderophores

