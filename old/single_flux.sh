#!/bin/bash

control_c() {
  echo -en "\n*** Ouch! Exiting ***\n"
  exit $?
}
trap control_c SIGINT

prod=$1

for aggk in `cat aggk.txt`
do
  >&2 echo "k = $aggk"
  echo "# k = $aggk"
  ./single_secretor_2.py --aggk $aggk --dt -0.0001 \
    --maxdt 1e8 --maxTime $(( 300*365*24*60*60 )) \
    --prod $prod --xmax 10.0 --nmesh 400 --report 0.0
  echo ""
done

