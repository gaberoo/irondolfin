#!/bin/bash

MESH_ID=8

./plotMesh2.R -i meshes/mesh_${MESH_ID}.xml.gz -o tmp-mesh-1.pdf
./plotMesh2.R -i meshes/mesh_${MESH_ID}.xml.gz -o tmp-mesh-2.pdf -X 1e-4
./plotMesh2.R -i meshes/mesh_${MESH_ID}.xml.gz -o tmp-mesh-3.pdf -X 1e-5

