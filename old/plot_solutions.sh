#!/bin/bash

cat $1 | while read one two
do
  ./plotMesh.R -d $two -i solutions/${2}_${one}.pvd \
    -o plts/${2}_${one}.pdf -X 4e-5 -Y 2e-5
done
