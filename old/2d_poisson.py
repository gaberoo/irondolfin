#!/Applications/FEniCS.app/Contents/Resources/bin/python

from mshr import *
from dolfin import *

# make the 2D mesh

p1 = Point(-4.,0.)
p2 = Point( 4.,0.)

xmax = 100.0
ymax = 50.0
r = 0.5

left  = Point(-xmax,-ymax)
right = Point( xmax, ymax)
rect = Rectangle(left,right)

C1 = Circle(p1,r)
C2 = Circle(p2,r)

domain = rect - C1 - C2
mesh = generate_mesh(domain,100)

# setup the Poisson equation

V = FunctionSpace(mesh, 'Lagrange', 1)

# Define boundary conditions
#u0 = Expression("1 + x[0]*x[0] + 2*x[1]*x[1]")
u0 = Constant(100.0)
zero = Constant(0.0)

def r2(x):
    return x[0]*x[0] + x[1]*x[1]

def outer_boundary(x):
    return abs(x[0]) >= xmax or abs(x[1]) >= ymax

def boundary_in(x, on_boundary):
    return on_boundary and not outer_boundary(x)

def boundary_out(x, on_boundary):
    return on_boundary and outer_boundary(x)

bc1 = DirichletBC(V, u0,   boundary_out)
bc2 = DirichletBC(V, zero, boundary_in)

u1 = interpolate(u0, V)

T = 1.9       # total simulation time
dt = 0.3      # time step

# Define variational problem
u = TrialFunction(V)
v = TestFunction(V)
f = Constant(0.0)
a = inner(nabla_grad(u), nabla_grad(v))*dx
L = f*v*dx

# Compute solution
u = Function(V)
solve(a == L, u, [bc1, bc2])

# Plot solution and mesh
plot(u,interactive=True)

#plot(mesh)


