#!/bin/bash

control_c() {
  echo -en "\n*** Ouch! Exiting ***\n"
  exit $?
}
trap control_c SIGINT

export FENICS_CONFIG_SILENT=1
source /Applications/FEniCS.app/Contents/Resources/share/fenics/fenics.conf

prod=$1
aggk=$2
maxDays=$3

./single_secretor.py --aggk $aggk --dt -0.0001 \
  --maxTime $(( $maxDays*24*60*60 )) --maxdt 100000 --plot \
  --prod $prod --xmax 10.0 --nmesh 400 --report 0.0

