#!/usr/bin/env python3

import os
os.environ['CC'] = 'gcc-8'
os.environ['CXX'] = 'g++-8'

from mshr import *
from dolfin import *

import numpy as np
import math

from constants import *

aggk = 1e4
DFe = Diff(aggk**(1./3.)*rFe)
DSid = Diff(rSid)

# make the 2D mesh

xmax = 1e-4
p1 = Point(0.0,0.0)
domain = Rectangle(Point(xmax,xmax),Point(-xmax,-xmax)) - Circle(p1,rB)

mesh = generate_mesh(domain,10)
for i in range(6):
    # Mark cells for refinement
    markers = MeshFunction("bool", mesh, mesh.topology().dim())
    markers.set_all(False)
    for cell in dolfin.cells(mesh):
        r1 = cell.midpoint().distance(p1)
        if r1 < xmax/(i+1.0):
            markers[cell.index()] = True
    mesh = refine(mesh, markers)

# setup the Poisson equation
V = FunctionSpace(mesh, 'Lagrange', 1)

# Define boundary conditions
u0 = Constant(1e-7)
zero = Constant(0.0)

def inner_boundary(x):
    eps = 1e-5
    r1 = sqrt((x[0]-p1[0])**2+(x[1]-p1[1])**2)
    return abs(r1-rB) < eps

class CellBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and inner_boundary(x)

class DomainBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and not inner_boundary(x)

bndry_cell = CellBoundary()
bndry_out = DomainBoundary()

boundaries = MeshFunction("size_t", mesh, mesh.topology().dim()-1)
boundaries.set_all(0)
bndry_cell.mark(boundaries,1)
bndry_out.mark(boundaries,2)

bcs = [ DirichletBC(V, u0,   boundaries, 2),
        DirichletBC(V, zero, boundaries, 1)]

# Setup measure for the boundaries (needed to calculate flux)
ds = Measure('ds', domain=mesh, subdomain_data=boundaries)

# Define variational problem
x = SpatialCoordinate(mesh)
u = TrialFunction(V)
v = TestFunction(V)
f = Constant(0.0)
r = Expression("x[1]", degree=1)
a = r * inner(DFe*grad(u),grad(v)) * dx
L = f*v*dx

# Compute solution
F = Function(V)

problem = LinearVariationalProblem(a, L, F, bcs)
solver = LinearVariationalSolver(problem)
solver.parameters["krylov_solver"]["absolute_tolerance"] = 1e-30
solver.parameters["krylov_solver"]["relative_tolerance"] = 5e-6
solver.parameters["krylov_solver"]["monitor_convergence"] = True
solver.solve()

# Total flux
n = FacetNormal(mesh)
p = Expression("pi*fabs(x[1])", degree=1)
flux = -p*dot(grad(u),n)*ds(1)
total_flux = assemble(flux)
print(total_flux*Na*DFe)

