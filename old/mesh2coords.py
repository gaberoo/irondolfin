from dolfin import *
import numpy as np
import cPickle as pickle

mesh = Mesh("2d_mesh.xml")

mesh_dict = dict()
mesh_dict["n"] = mesh.num_vertices()
mesh_dict["d"] = mesh.geometry().dim()
mesh_dict["coords"] = mesh.coordinates()
mesh_dict["triangles"] = np.asarray([cell.entities(0) for cell in cells(mesh)])

pickle.dump( mesh_dict, open("2d_mesh.p","wb") )


