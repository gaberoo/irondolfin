#!/bin/bash

control_c() {
  echo -en "\n*** Ouch! Exiting ***\n"
  exit $?
}
trap control_c SIGINT
 
source /Applications/FEniCS.app/Contents/Resources/share/fenics/fenics.conf

#for nmesh in 10 20 50 100 200 500 1000 2000 5000
#do
#  ./single_uptake_time.py --dt 100.0 --aggk 100.0 --nmesh $nmesh --verbose 0
#done
#echo -n "\n\n\n"

for dt in 200.0 100.0 50.0 20.0 10.0 5.0 2.0 1.0 
do
  ./single_uptake_time.py --dt $dt --aggk 100.0 --nmesh 200 --verbose 0
done


