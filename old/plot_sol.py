from dolfin import *
import matplotlib.pyplot as plt
import matplotlib.tri as tri
import numpy as np
import cPickle as pickle

mesh = Mesh("2d_mesh.xml")

n = mesh.num_vertices()
d = mesh.geometry().dim()
coords = mesh.coordinates()
triangles = np.asarray([cell.entities(0) for cell in cells(mesh)])

triangulation = tri.Triangulation(coords[:, 0],coords[:, 1],triangles)

V = FunctionSpace(mesh, 'Lagrange', 1)
u = Function(V)
file_in = File("2d_poisson_space.pvd")
file_in >> u

#f_exp = Expression('sin(2*pi*(x[0]*x[0]+x[1]*x[1]))')
#f = interpolate(f_exp, V)

# Get the z values as face colors for each triangle(midpoint)
#plt.figure()
#zfaces = np.asarray([f(cell.midpoint()) for cell in cells(mesh)])
#plt.tripcolor(triangulation, facecolors=zfaces, edgecolors='k')
#plt.savefig('f0.png')
#
# Get the z values for each vertex
#plt.figure()
#z = np.asarray([f(point) for point in mesh_coordinates])
#plt.tripcolor(triangulation, z, edgecolors='k')
#plt.savefig('f1.png')

# Comment to prevent pop-up
#plt.show()
