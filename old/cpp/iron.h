#ifndef __IRON_H__
#define __IRON_H__

const double kB = 1.38e-23;
const double Temp = 293.0;
const double h = 1.003e-3;
const double Na = 6.023e23;
const double rFe = 1e-10;

class Params {
public:
  inline double Diff(double r) {
    return kB*Temp/(6.0*M_PI*h*r);
  }

  inline double DSid() { return Diff(rSid); }

  inline double DFe(double k) {
    double r = pow(k,1.0/3.0)*rFe;
    return Diff(r);
  }

  double dt = 1e-2;
  double maxTime = 100.0;
  double aggk = 1e4;
  double Fe0 = 1e-7;
  double rB = 1e-6;
  double rSid = 1e-9;
  double xmax = 0.1;
  double eps = 1e-7;
  double kappa = 1e6;
  double sidProd = 1e-9;
  double reportTime = 0.1;

  string meshFile = "meshes/linear.xml.gz";
};


#endif
