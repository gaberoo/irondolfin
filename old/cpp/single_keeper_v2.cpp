#include <iostream>
#include <string>
#include <map>
using namespace std;

#include <dolfin.h>
#include "SingleKeeper.h"
using namespace dolfin;

#include "iron.h"

// Initial conditions
class InitialConditions : public Expression {
public:
  InitialConditions(double Fe0) 
    : Expression(1), Fe0(Fe0)
  {}

  void eval(Array<double>& vals, const Array<double>& x) const {
    vals[0] = Fe0;
  }

  double Fe0 = 0.0;
};

class SecretorsEquation : public NonlinearProblem {
  public:
    SecretorsEquation(shared_ptr<const Form> F,
                      shared_ptr<const Form> J,
                      shared_ptr<vector<const DirichletBC*>> bcs) 
      : _F(F), _J(J), _bcs(bcs) 
    {}

    // User defined residual vector
    void F(GenericVector& b, const GenericVector& x)
    { 
      assemble(b, *_F); 
      for (auto bc: *_bcs) bc->apply(b, x);
    }

    // User defined assemble of Jacobian
    void J(GenericMatrix& A, const GenericVector& x)
    { 
      assemble(A, *_J); 
      for (auto bc: *_bcs) bc->apply(A);
    }

  private:
    shared_ptr<const Form> _F;
    shared_ptr<const Form> _J;
    shared_ptr<vector<const DirichletBC*>> _bcs;
};

// SubDomain for the cell
class CellBoundary1D : public SubDomain {
  public:
    CellBoundary1D(double rB) : _rB(rB) {}

    bool inside(const Array<double>& x, bool on_boundary) const
    { return on_boundary && x[0] < 1.5*_rB; }

  private:
    double _rB = 1e-6;
};

// SubDomain for the 'infinity' boundary
class DomainBoundary1D : public SubDomain {
  public:
    DomainBoundary1D(double xmax) : _xmax(xmax) {}

    bool inside(const Array<double>& x, bool on_boundary) const
    { return on_boundary && x[0] > 0.9*_xmax; }

  private:
    double _xmax = 0.1;
};

int main(int argc, char** argv) {
  int verbose = 1;

  if (verbose) cerr << "This is '" << argv[0] << "'." << std::endl;

  Params p;

  // Read mesh

  if (verbose) cerr << "Reading mesh." << std::endl;
  auto mesh = make_shared<Mesh>(p.meshFile.c_str());

  // Mark subdomains

  auto boundaries = make_shared<MeshFunction<std::size_t>>(mesh, mesh->topology().dim()-1);
  *boundaries = 0;

  auto bndry_cell = make_shared<CellBoundary1D>(p.rB);
  bndry_cell->mark(*boundaries, 1);

  auto bndry_out = make_shared<DomainBoundary1D>(p.xmax);
  bndry_out->mark(*boundaries, 2);

  // Create function space and forms

  if (verbose) cerr << "Creating function space." << std::endl;

  auto W = make_shared<SingleKeeper::FunctionSpace>(mesh);

  shared_ptr<Form> L    = make_shared<SingleKeeper::ResidualForm>(W);
  shared_ptr<Form> dL   = make_shared<SingleKeeper::JacobianForm>(W, W);
  shared_ptr<Form> flux = make_shared<SingleKeeper::Functional>(mesh);

  // Create solution Functions (at t_n and t_{n+1})

  if (verbose) cerr << "Creating solution functions." << std::endl;

  auto u0 = make_shared<Function>(W);
  auto u  = make_shared<Function>(W);

  // Set solution to intitial condition

  if (verbose) cerr << "Applying initial conditions." << std::endl;

  InitialConditions u_initial(p.Fe0);
  *u  = u_initial;
  *u0 = u_initial;

  // Time stepping and model parameters

  if (verbose) cerr << "Setting model parameters." << std::endl;

  auto dt  = make_shared<Constant>(p.dt);
  auto DFe = make_shared<Constant>(p.DFe(p.aggk));
  auto Pi  = make_shared<Constant>(M_PI);

  auto Fe = make_shared<Constant>(p.Fe0);
  auto zero = make_shared<Constant>(0.0);

  // Set coefficients and attach to forms

  map<string,shared_ptr<const GenericFunction>> coefsL;
  coefsL["u"]       = u;
  coefsL["u0"]      = u0;
  coefsL["dt"]      = dt;
  coefsL["DFe"]     = DFe;

  map<string,shared_ptr<const GenericFunction>> coefsdL;
  coefsdL["u"]      = u;
  coefsdL["dt"]      = dt;
  coefsdL["DFe"]     = DFe;

  map<string,shared_ptr<const GenericFunction>> coefsFlux;
  coefsFlux["Pi"] = Pi;
  coefsFlux["u"]  = u;

  if (verbose) cerr << "Attaching coefficients." << std::flush;

  if (verbose) cerr << " ...L" << std::flush;
  L->set_coefficients(coefsL);
  L->ds = boundaries;

  if (verbose) cerr << " ...dL" << std::flush;
  dL->set_coefficients(coefsdL);
  dL->ds = boundaries;

  if (verbose) cerr << " ...flux" << std::flush;
  flux->set_coefficients(coefsFlux);
  flux->ds = boundaries;

  if (verbose) cerr << " ...done." << std::endl;

  // Boundary conditions

  DirichletBC bc_out_F (W, Fe,   bndry_out);
  DirichletBC bc_cell_F(W, zero, bndry_cell);

  auto bcs = make_shared<vector<const DirichletBC*>>();
  bcs->push_back(&bc_out_F);
  bcs->push_back(&bc_cell_F);

  // Create nonlinear solver and set parameters

  if (verbose) cerr << "Creating solver." << std::endl;

  SecretorsEquation secretors(L, dL, bcs);

  NewtonSolver solver;
  solver.parameters["linear_solver"] = "iterative";
  solver.parameters["convergence_criterion"] = "incremental";
  solver.parameters["relative_tolerance"] = 1e-30;
  solver.parameters["absolute_tolerance"] = 1e-30;
  solver.parameters["maximum_iterations"] = 10;

  // Solve equations

  double t = 0.0;
  double next_print = p.reportTime;
  double total_iron = 0.0;

  Vector uL;
  assemble(uL,*L);

  // solve(*L == 0.0, *u, *bcs, *dL);
  solver.solve(secretors, *u->vector());

  return 0;

  /*
  while (t < p.maxTime) {
    t += abs(*dt);

    *(u0->vector()) = *(u->vector());

    solve(*L == 0.0, *u, *bcs, *dL);

    // solver.solve(secretors, *u->vector());

    const Vector& uu = *u->vector();
    double total_flux = assemble(*flux)*Na*(*DSid);
    std::cout << t << " " 
              << total_flux << " " 
              << uu[2] << " "
              << std::endl;
  }

  return 0;
  */
}
