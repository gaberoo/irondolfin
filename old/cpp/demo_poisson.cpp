#include <iostream>
#include <vector>

#include <dolfin.h>
#include "Poisson.h"

using namespace dolfin;

class Source : public Expression {
  void eval(Array<double>& values, const Array<double>& x) const {
    double dx = x[0] - 0.5;
    double dy = x[1] - 0.5;
    values[0] = 10*exp(-(dx*dx + dy*dy) / 0.02);
  }
};

class dUdN : public Expression
{
  void eval(Array<double>& values, const Array<double>& x) const {
    values[0] = sin(5*x[0]);
  }
};

class DirichletBoundary : public SubDomain {
  bool inside(const Array<double>& x, bool on_boundary) const {
    return x[0] < DOLFIN_EPS or x[0] > 1.0 - DOLFIN_EPS;
  }
};

int main() {
  // Create mesh and function space
  auto mesh = std::make_shared<Mesh>(
    UnitSquareMesh::create({{32, 32}}, CellType::Type::triangle));

  auto V = std::make_shared<Poisson::FunctionSpace>(mesh);

  // Define boundary condition
  auto u0 = std::make_shared<Constant>(0.0);
  auto boundary = std::make_shared<DirichletBoundary>();
  DirichletBC bc(V, u0, boundary);

  // Define variational forms
  Poisson::BilinearForm a(V, V);
  Poisson::LinearForm L(V);
  auto f = std::make_shared<Source>();
  auto g = std::make_shared<dUdN>();
  L.f = f;
  L.g = g;

  // Compute solution
  Function u(V);

  NewtonSolver solver;
  solver.solve(a == L, u, bc);

  std::vector<double> vu;
  u.compute_vertex_values(vu,*mesh);

  auto coords = mesh->coordinates();
  int n = vu.size();

  double last = 0.0;
  for (int i = 0; i < n; ++i) {
    if (coords[2*i] < last) std::cout << std::endl;
    last = coords[2*i];
    std::cout << i << " " 
              << coords[2*i] << " "
              << coords[2*i+1] << " "
              << vu[i] << std::endl;
  }

  return 0;
}
