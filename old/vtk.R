library(XML)

.str.trim <- function (x) gsub("^\\s+|\\s+$", "", x)

.vtk.DataArray <- function(p,nc=NA,compression=NULL) {
  att <- as.list(xmlAttrs(p))
  nc <- att$NumberOfComponents
  if (is.null(nc)) { nc <- 1 } else { nc <- as.integer(nc) }
  if (att$format == "binary") {
    vdata <- .str.trim(xmlValue(p))
  } else {
    vdata <- as.numeric(strsplit(xmlValue(p),"\\s+")[[1]])
    if (nc > 1) vdata <- matrix(vdata,ncol=nc,byrow=TRUE)
    return(vdata)
  }
}

read.vtk <- function(filename) {
  doc <- xmlParse(filename)
  root <- xmlRoot(doc)
  data.sets <- xmlSApply(root[["Collection"]],function(x) {
    att <- as.list(xmlAttrs(x))
  })

  ds <- r[["Collection"]][["DataSet"]]
  fn2 <- unname(xmlAttrs(ds)["file"])
  doc2 <- xmlTreeParse(sprintf("%s/%s",dirname(filename),fn2))
  r <- xmlRoot(doc2)
# Vertices
  p <- r[["UnstructuredGrid"]][["Piece"]][["Points"]][["DataArray"]]
  vcoords <- .pvd.DataArray(p)
  ids <- 0:(nrow(vcoords)-1)
  vertices <- cbind(ids,vcoords)
  colnames(vertices) <- c("idx","x","y","z")
# Cells
  p <- r[["UnstructuredGrid"]][["Piece"]][["Cells"]]
  names(p) <- xmlSApply(p,function(x) xmlAttrs(x)["Name"])
  tri <- .pvd.DataArray(p[["connectivity"]],nc=3)
  cells <- cbind(1:nrow(tri),tri)
# Values
  p <- r[["UnstructuredGrid"]][["Piece"]][["PointData"]][["DataArray"]]
  vdata <- .pvd.DataArray(p)
}

read.vtu <- function(filename) {
  doc <- xmlParse(filename)
  root <- xmlRoot(doc)
  att <- as.list(xmlAttrs(root))
  rtype <- att$type
  dat <- root[[rtype]]
  p <- dat[["Piece"]][["PointData"]][["DataArray"]]
  .vtk.DataArray(p)
}


