#!/bin/bash

control_c() {
  echo -en "\n*** Ouch! Exiting ***\n"
  exit $?
}
trap control_c SIGINT

#export FENICS_CONF_SILENT=1
#source /Applications/FEniCS.app/Contents/Resources/share/fenics/fenics.conf

while read one two
do 
  echo "$one -- $two"
  #./rect_mesh.py --size 10 --output meshes/$2_${one}.xml.gz \
  #--refine 6 --verbose 20 --xmax 1e-4 --ymax 5e-5 $two
  ./makeMesh.R -R 1e-1 -o meshes/mesh_${one}.xml -m 24 -d $two
  gzip -f meshes/mesh_${one}.xml
done
