#!/bin/bash

control_c() {
  echo -en "\n*** Ouch! Exiting ***\n"
  exit $?
}
trap control_c SIGINT

w=$1
prod=$2

for aggk in $( cat aggk.txt )
do
  >&2 echo "k = $aggk, Xr = $prod, w = $w"
  ./single_secretor_2.py --aggk $aggk \
    --dt -0.001 --maxdt 1e10 --maxTime 1e12 \
    --prod $prod --xmax 10 --nmesh 400 \
    --output data/dist_$aggk
done

