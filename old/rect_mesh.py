#!/usr/bin/env python2.7

import argparse

from mshr import *
from dolfin import *
from iron import *

import matplotlib.pyplot as plt
import matplotlib.tri as tri
import numpy as np

parser = argparse.ArgumentParser(description='Make mesh for linear cells.')
parser.add_argument('-d', '--distance', metavar='d', type=float, default=1e-5,
                   help='distance between the cells (centers)')
parser.add_argument('-X','--xmax', metavar='X', type=float, default=1e-4,
                    help = 'Length of x-dimension')
parser.add_argument('-Y','--ymax', metavar='Y', type=float, default=1e-4,
                    help = 'Length of y-dimension')
parser.add_argument('--xview', metavar='X', type=float, default=None,
                    help = 'x viewport')
parser.add_argument('--yview', metavar='X', type=float, default=None,
                    help = 'y viewport')
parser.add_argument('-O',"--output", metavar="file", default="mesh.xml.gz",
                    help="Output filename")
parser.add_argument("--refine", metavar="number", type=int, default=3,
                    help="Number of mesh refinements")
parser.add_argument("-v","--verbose", metavar="level", type=int, default=40,
                    help="Logging level")
parser.add_argument("--size", metavar="size", type=int, default=30,
                    help="Mesh size")
parser.add_argument('--expandMin', metavar='len', type=float, default=1e-5,
                    help='Grid expandsion above this lenth')
parser.add_argument('--factor', metavar='expontent', type=float, default=2.0,
                    help='Grid expandsion factor')
parser.add_argument("--plot", action="store_true")
parser.add_argument("--rect", action="store_true")
args = parser.parse_args()

# make the 2D mesh

xmax = args.xmax
ymax = args.ymax
rad = rB

mesh, xmax, ymax = make_mesh(args.distance,xmax,ymax,args.size,
                             args.refine,rad,args.rect,
                             args.factor,args.expandMin)

#xmax = 1e-3
#ymax = 1e-3
#size = 20
#refine = True
#expandMin = 4e-5
#distance = 1e-5
#mesh = make_mesh(distance,xmax,ymax,size,refine,
#                 rad,False,3.0,expandMin)
#plot_mesh(mesh)

if args.plot:
    plot_mesh(mesh,args.xview,args.yview)
    raw_input("Press Enter to continue...")
    #plot_mesh(mesh,2*args.expandMin,2*args.expandMin)
else:
    print "Writing mesh to file '%s'..." % args.output
    File(args.output) << mesh


