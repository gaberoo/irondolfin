#!/usr/bin/env python2.7

import sys
import signal
import argparse
import math
import numpy

##############################################################################
# Set signal handler

def signal_handler(signal, frame):
  print("Exiting...")
  sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)

##############################################################################
# Write to file

def write(out, arr, time=0.0, log10=False):
  if time != None:
    out.write("%.12g " % time)

  for x in arr:
    if log10:
      if x > 0.0:
        out.write("%.12g " % math.log10(x))
      else:
        out.write("-Inf ")
    else:
      out.write("%.12g " % x)

  out.write("\n")
  out.flush()

##############################################################################
# FEniCS libraries

import mshr

import dolfin
from dolfin import inner, nabla_grad, dx

import iron
import iron_4eq as I

args = iron.parser.parse_args()

##############################################################################
# Numpy/Matplotlib libraries

if args.plot:
  import ironPlot
  import matplotlib as mpl
  mpl.rcParams['toolbar'] = 'None'

##############################################################################
# Form compiler options

dolfin.parameters["form_compiler"]["optimize"]     = True
dolfin.parameters["form_compiler"]["cpp_optimize"] = True
dolfin.parameters["form_compiler"]["representation"] = "quadrature"
dolfin.set_log_level(dolfin.WARNING)

##############################################################################

secEq = I.SecretorEqs(dt=args.dt, maxTime=numpy.inf,
                      k=args.aggk, xmax=args.xmax,
                      kX=args.kappaFX, kL=args.kappaEY,
                      prod=args.prod, rho=1e-7,
                      output=args.output)
secEq.generateMesh(nmesh=args.nmesh, scale=args.scale)
secEq.defineFunctionSpace()
secEq.defineBoundaries()
secEq.defineVariationalProblem(dim=3, eps=0.0)
secEq.createNonlinearSolver()
secEq.run(maxdt=args.maxdt, plot=args.plot,
          reportTime=args.reportTime, critIron=1e6)


