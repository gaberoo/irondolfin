#!/usr/bin/env python2.7

import sys
import signal
import argparse

##############################################################################
# Set signal handler

def signal_handler(signal, frame):
    print("Exiting...")
    sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)

##############################################################################
# Numpy/Matplotlib libraries

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
mpl.rcParams['toolbar'] = 'None'

##############################################################################
# FEniCS libraries

from mshr import *
from dolfin import *
from iron import *

parser = argparse.ArgumentParser(description='Iron diffusion with two cells.')
parser.add_argument('--nmesh', metavar='points', type=int, default=100,
                    help="Number of mesh points")
parser.add_argument('--scale', metavar='value', type=float, default=4.0,
                    help="Mesh stretching parameter")
parser.add_argument('--aggk', metavar='k', type=float, default=1.0,
                    help='Aggregation')
parser.add_argument('--dt', metavar='time', type=float, default=1.0,
                    help='Time step')
parser.add_argument('--maxdt', metavar='time', type=float, default=1000.0,
                    help='Max time step')
parser.add_argument('--prod', metavar='rate', type=float, default=SidProd,
                    help='Siderophore production rate')
parser.add_argument('--xmax', metavar='pos', type=float, default=0.1,
                    help='Maximum radial distance')
parser.add_argument('--kappa', metavar='kappa', type=float, default=1e6,
                    help='Binding rate Sid-Fe')
parser.add_argument("--verbose", metavar="level", type=int, default=1,
                    help="Logging level")
parser.add_argument("--plot", action="store_true")
args = parser.parse_args()

##############################################################################
# Form compiler options

parameters["form_compiler"]["optimize"]     = True
parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["representation"] = "quadrature"

set_log_level(WARNING)

##############################################################################
# Some parameters

dt   = Constant(args.dt)       # time step
aggk = Constant(args.aggk)     # iron aggregate size
fDFe  = Diff(aggk**(1./3.)*rFe) # diffusion coefficient of iron
fdt  = args.dt
Fe0  = 1e-7

xmax = args.xmax
eps  = 1e-7
rad  = rB
crit_iron = 1e6
Rstar = args.prod*rB**2/(fDFe*Fe0)

##############################################################################
# make the 1D mesh

if args.verbose > 1:
    sys.stderr.write("Generating mesh...")
mesh = IntervalMesh(args.nmesh,rB,xmax)
coords = expand(mesh.coordinates(),rB,xmax,args.scale)
mesh.coordinates()[:] = coords
if args.verbose > 1:
    sys.stderr.write(" minimal distance = %g\n" % ( coords[1]-coords[0] ))

##############################################################################
# Define the function space

#V = FunctionSpace(mesh, "CG", 1)  # CG = Lagrange
#W = MixedFunctionSpace([V,V,V])
P1 = FiniteElement("CG", interval, 1) # use interval because it's 1D
element = MixedElement([P1, P1, P1])
W = FunctionSpace(mesh, element)

##############################################################################
# Set the time step

if fdt < 0.0:
    sys.stderr.write("Increasing dt selected.\n")
    dt = Expression("dt",dt=-fdt,element=P1)
else:
    dt = Expression("dt",dt=fdt,element=P1)

##############################################################################
# Define boundaries

Fe = Constant(Fe0)
XProd = Constant(1e-6)
zero = Constant(0.0)

class CellBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and x < rB*1.5

class DomainBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and x > xmax*.9

boundaries = FacetFunction("size_t", mesh)
boundaries.set_all(0)

bndry_out  = DomainBoundary()
bndry_cell = CellBoundary()

bndry_cell.mark(boundaries,1)
bndry_out.mark(boundaries,2)

bc_out_F  = DirichletBC(W.sub(0), Fe,    bndry_out)
#bc_cell_F = DirichletBC(W.sub(0), zero,  bndry_cell)

bc_out_X  = DirichletBC(W.sub(1), zero,  bndry_out)
#bc_cell_X = DirichletBC(W.sub(1), XProd, bndry_cell)

bc_out_Y  = DirichletBC(W.sub(2), zero,  bndry_out)
bc_cell_Y = DirichletBC(W.sub(2), zero,  bndry_cell)

bcs = [ bc_out_F, bc_out_X, bc_out_Y, bc_cell_Y ]

##############################################################################
# Setup measure for the boundaries (needed to calculate flux)

ds = Measure('ds', domain=mesh, subdomain_data=boundaries)

##############################################################################
# Define variational problem

# Define trial and test functions
du = TrialFunction(W)
u  = Function(W)
u0 = Function(W)
vF, vX, vY = TestFunctions(W)

# Define functions

# Split mixed functions
F,  X,  Y  = split(u)
F0, X0, Y0 = split(u0)

# Create intial conditions and interpolate
#u_init = InitialConditions(Fe0,element=P1)
u_init = Expression(("Fe-eps","eps","eps"), Fe=Fe0, eps=0.0, degree=1)
u.interpolate(u_init)
u0.interpolate(u_init)

f = Constant(0.0)
kappa = Expression("kappa", kappa=args.kappa, degree=1)

r2  = Expression("x[0]*x[0]", element=P1)
gX  = Constant(args.prod)
DFe = Expression("kB*Temp/(6.*pi*h*pow(aggk,1./3.)*rFe)",
                 element=P1,
                 kB=kB,Temp=Temp,h=h,aggk=aggk,rFe=rFe)
fXY = kappa*X*F

aF = dt*r2*DFe *inner(nabla_grad(F), nabla_grad(vF)) * dx
aX = dt*r2*DSid*inner(nabla_grad(X), nabla_grad(vX)) * dx
aY = dt*r2*DSid*inner(nabla_grad(Y), nabla_grad(vY)) * dx 
a0 = aF + aX + aY

Lg = dt*r2*gX*vX*ds
L0 = - dt*r2*fXY*vF*dx - dt*r2*fXY*vX*dx + dt*r2*fXY*vY*dx 

U1 = r2* F*vF*dx + r2* X*vX*dx + r2* Y*vY*dx
U0 = r2*F0*vF*dx + r2*X0*vX*dx + r2*Y0*vY*dx

L = (U1-U0) + (a0-L0-Lg)
dL = derivative(L, u, du)

n = FacetNormal(mesh)
p = Expression("4.0*pi*x[0]*x[0]", element=P1)
flux = -p*dot(nabla_grad(Y), n)*ds(1)

##############################################################################
# Create nonlinear problem and Newton solver

problem = SecretorsEquation(dL, L, bcs)
solver = NewtonSolver()
solver.parameters["linear_solver"] = "lu"
solver.parameters["convergence_criterion"] = "incremental"
solver.parameters["relative_tolerance"] = 1e-10

t = 0.0
total_iron = 0.0

if args.plot:
    log_coords = [ math.log10(z) for z in coords ]
    y = dolf2nump(u, mesh)
    fig, (ax1, ax2, ax3) = plt.subplots(3, sharex=True)
    ax1.set_ylim([0, 1e-7])
    ax2.set_ylim([0, args.prod**0.6])
    ax3.set_ylim([0, args.prod**0.9])
    ax1.axvline(math.log10(Rstar), color="grey")
    ax2.axvline(math.log10(Rstar), color="grey")
    ax3.axvline(math.log10(Rstar), color="grey")
    p1, = ax1.plot(log_coords,y[:,0], color="#4daf4a", linewidth=2.0)
    p2, = ax2.plot(log_coords,y[:,1], color="#ff7f00", linewidth=2.0)
    p3, = ax3.plot(log_coords,y[:,2], color="#984ea3", linewidth=2.0)
    plt.show(block=False)
    plt.pause(0.01)

final_time = float("inf")
while total_iron < 1e6:
    t += abs(fdt)
    u0.assign(u)
    solver.solve(problem, u.vector())
    total_flux = assemble(flux) * Na*DSid
    new_total_iron = total_iron + abs(fdt)*total_flux
    if new_total_iron > crit_iron:
        diff = (crit_iron-total_iron)/(new_total_iron-total_iron)
        final_time = (t-abs(fdt)) + diff*abs(fdt)
        total_iron = new_total_iron
        break
    else:
        total_iron = new_total_iron
        if args.verbose > 1:
            sys.stderr.write("t = %g, iron = %g (%5.2f%%)         \r" % (t,total_iron,100*total_iron/crit_iron))
        if args.plot:
            y = dolf2nump(u,mesh)
            p1.set_data(log_coords,y[:,0])
            p2.set_data(log_coords,y[:,1])
            p3.set_data(log_coords,y[:,2])
            plt.draw()
            plt.pause(0.01)
    if fdt < 0.0:
        fdt = fdt * 1.2
        if abs(fdt) > args.maxdt:
            fdt = args.maxdt
            dt.dt = fdt
        else:
            dt.dt = -fdt
        if args.verbose > 2:
            sys.stderr.write("New dt = %g\n" % fdt)

if args.verbose > 1:
    sys.stderr.write("Done.                                          \n")

print args.nmesh, args.prod, float(aggk), fdt, final_time

if args.plot:
    print "Final time = %f" % final_time
    tmp = raw_input('Enter to continue.')

