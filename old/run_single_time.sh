#!/bin/bash

control_c() {
  echo -en "\n*** Ouch! Exiting ***\n"
  exit $?
}
trap control_c SIGINT

w=$1

for prod in `cat prod_rates_2.txt | sort -rg`
do
  echo "# p = $prod"
  for aggk in `cat aggk.txt`
  do
#  echo "# k = $aggk"
#    for w in `cat weathering.txt | sort -rg`
#    do
      >&2 echo "k = $aggk, Xr = $prod, w = $w"
      ./single_uptake_time_2.py --dt -0.01 --aggk $aggk \
        --maxdt 1e4 --nmesh 400 --xmax 10.0 --prod $prod \
        --kappaEY $w
#    done
  done
  echo ""
done

