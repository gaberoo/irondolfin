#!/bin/bash

source /Applications/FEniCS.app/Contents/Resources/share/fenics/fenics.conf

echo "# Flux solutions to $2" > flux_${2}.txt

cat $1 | while read one two
do
  echo -n "$one $two " | tee -a flux_${2}.txt
  ./keepers.py --output solutions/${2}_${one} \
    meshes/${2}_${one}.xml.gz $two | tee -a flux_${2}.txt
done
