#!/usr/bin/env python2.7

import argparse
parser = argparse.ArgumentParser(description='Iron diffusion with two cells.')
parser.add_argument('mesh', metavar='meshfile', help="Mesh filename")
parser.add_argument('distance', metavar='d', type=float,
                   help='distance between the two cells (centers)')
parser.add_argument('--aggk', metavar='k', type=int, default=1,
                    help='Aggregation')
parser.add_argument("--output", metavar="file", default=None,
                    help="Output filenames")
parser.add_argument("--verbose", metavar="level", type=int, default=40,
                    help="Logging level")
args = parser.parse_args()

from mshr import *
from dolfin import *
import numpy as np
import math

from constants import *

set_log_level(args.verbose)

aggk = args.aggk
DFe = Diff(aggk**(1./3.)*rFe)
DSid = Diff(rSid)

# make the 2D mesh

p1 = Point( args.distance/2.0,0.)
p2 = Point(-args.distance/2.0,0.)

mesh = Mesh(args.mesh)

# setup the Poisson equation
V = FunctionSpace(mesh, 'Lagrange', 1)

# Define boundary conditions
u0 = Constant(1e-7)
zero = Constant(0.0)

def outer_boundary(x):
    return near(abs(x[0]),xmax) or near(abs(x[1]),ymax)
    #return abs(x[0]) >= xmax or abs(x[1]) >= ymax

def inner_boundary(x):
    eps = 1e-5
    r1 = sqrt((x[0]-p1[0])**2+(x[1]-p1[1])**2)
    r2 = sqrt((x[0]-p2[0])**2+(x[1]-p2[1])**2)
    return abs(r1-rB) < eps or abs(r2-rB) < eps

class CellBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and inner_boundary(x)

class DomainBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and not inner_boundary(x)

bndry_cell = CellBoundary()
bndry_out = DomainBoundary()

boundaries = FacetFunction("size_t", mesh)
boundaries.set_all(0)
bndry_cell.mark(boundaries,1)
bndry_out.mark(boundaries,2)

bcs = [ DirichletBC(V, u0,   boundaries, 2),
        DirichletBC(V, zero, boundaries, 1)]

# Setup measure for the boundaries (needed to calculate flux)
ds = Measure('ds', domain=mesh, subdomain_data=boundaries)

# Define variational problem
x = SpatialCoordinate(mesh)
u = TrialFunction(V)
v = TestFunction(V)
f = Constant(0.0)
r = Expression("fabs(x[1])", degree=1)
a = r*inner(DFe*grad(u),grad(v)) * dx
L = f*v*dx

# Compute solution
u = Function(V)

problem = LinearVariationalProblem(a, L, u, bcs)
solver = LinearVariationalSolver(problem)
#solver = KrylovSolver("gmres", "ml_amg")
#solver.parameters["relative_tolerance"] = 5e-6
#solver.parameters["maximum_iterations"] = 1000
#solver.parameters["monitor_convergence"] = True
#solver.parameters["linear_solver"] = "lu"
solver.solve()

#A = assemble(a)
#b = assemble(L)
#solver = KrylovSolver(A,"gmres")
#solver.parameters["monitor_convergence"] = True
#solver.solve(u.vector(), b)

#solve(a == L, u, bcs)

# Save solutiont to a file
if args.output != None:
    File("%s.pvd" % args.output) << u
    File("%s-u.xml.gz" % args.output) << u

# Total flux
n = FacetNormal(mesh)
p = Expression("pi*fabs(x[1])", degree=1)
flux = -p*dot(nabla_grad(u),n)*ds(1)
total_flux = assemble(flux)
print total_flux*Na*DFe

