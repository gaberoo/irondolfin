
class IronCellBoundary : public SubDomain
{
public:

  bool inside(const Array<double>& x, bool on_boundary) const
  {
    return on_boundary && fabs(x[0]) - (0.5*distance+rB)
    if ((*materials)[cell.index] == 0)
      values[0] = k_0;
    else
      values[0] = k_1;
  }

  bool inner_boundary(const Array<double>& x, 
                    cells, radius, eps):
  rad = [ math.sqrt( (x[0]-p[0])**2 + (x[1]-p[1])**2 ) for p in cells ]
  on_b = [ (abs(r-radius) < eps) for r in rad ]
  return any(on_b)

  std::shared_ptr<MeshFunction<std::size_t>> materials;
  double distance;
  double rB;
};

