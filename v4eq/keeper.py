#!/usr/bin/env python3

import os
import sys
import signal
import argparse
import math
import numpy

os.environ['CC'] = 'gcc-8'
os.environ['CXX'] = 'g++-8'

##############################################################################
# Set signal handler

def signal_handler(signal, frame):
  print("Exiting...")
  sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)

##############################################################################
# FEniCS libraries

from dolfin import *
import mshr

import iron
args = iron.parser.parse_args()

##############################################################################
# Numpy/Matplotlib libraries

import matplotlib as mpl
mpl.rcParams['toolbar'] = 'None'
import matplotlib.pyplot as plt

##############################################################################
# Form compiler options

parameters["form_compiler"]["optimize"]     = True
parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["representation"] = "uflacs"
set_log_level(LogLevel.WARNING)

##############################################################################
# Some parameters

fdt  = args.dt
T    = args.maxTime         # total simulation time
aggk = Constant(args.aggk)  # iron aggregate size
Fe0  = 1e-7

#Rstar, fDFe = iron.R_star(args.prod, aggk, Fe0)
Rstar, fDFe = iron.R_star_2(args.prod, aggk, args.kappaFX, args.kappaEY)

if fdt < 0.0:
  sys.stderr.write("Increasing dt selected.\n")
  dt = Expression("dt", dt=-fdt, degree=1)
else:
  dt = Expression("dt", dt=fdt, degree=1)

xmax = args.xmax
eps  = 1e-7
rad  = iron.rB

##############################################################################
# make the 1D mesh

if args.verbose > 0:
  sys.stderr.write("Generating mesh...")
mesh = IntervalMesh(args.nmesh, iron.rB, xmax)
coords = iron.expand(mesh.coordinates(), iron.rB, xmax, args.scale)
mesh.coordinates()[:] = coords
if args.verbose > 0:
  sys.stderr.write("done. Minimum distance = %g.\n" % \
                   (coords[1]-coords[0]))

##############################################################################
# Define the function space

#V = FunctionSpace(mesh, "CG", 1)  # CG = Lagrange
P1 = FiniteElement("CG", interval, 1)
V = FunctionSpace(mesh, P1)

##############################################################################
# Define boundaries

Fe = Constant(Fe0)
zero = Constant(0.0)

boundaries = MeshFunction("size_t", mesh, 0)
boundaries.set_all(0)

bndry_cell = CompiledSubDomain("on_boundary && x[0] == rB", rB=iron.rB)
bndry_out  = CompiledSubDomain("on_boundary && x[0] == xmax", xmax=xmax)

bndry_cell.mark(boundaries, 1)
bndry_out.mark(boundaries, 2)

bc_out_F  = DirichletBC(V, Fe,   bndry_out)
bc_cell_F = DirichletBC(V, zero, bndry_cell)

bcs1 = [ bc_out_F, bc_cell_F ]
bcs2 = [ bc_out_F ]

##############################################################################
# Setup scalar variables

k = float(aggk)
D1 = iron.Diff(iron.rFe)
Rk = iron.rB + iron.rFe*(k**(1/3))
Ik = 0.0
Ik_max = 3*math.pi*Fe0/k*Rk*D1/args.kappaEY
KIk = Ik_max/100.0

coords = mesh.coordinates()
target = [ Fe0*(1-iron.rB/r) for r in coords ]

##############################################################################
# Setup measure for the boundaries (needed to calculate flux)

ds = Measure('ds', domain=mesh, subdomain_data=boundaries)

##############################################################################
# Define variational problem

# Define trial and test functions
dF = TrialFunction(V)
F  = Function(V)
F0 = Function(V)
vF = TestFunction(V)

dE = TrialFunction(V)
E  = Function(V)
E0 = Function(V)
vE = TestFunction(V)

n = FacetNormal(mesh)

# initial conditions

F.interpolate(Fe)
F0.interpolate(Fe)

# Define functions

DFe = Expression("kB*Temp/(6.*pi*h*pow(aggk,1./3.)*rFe)",
                 element=P1, kB=iron.kB, Temp=iron.Temp,
                 h=iron.h, aggk=aggk, rFe=iron.rFe)

n = FacetNormal(mesh)

if args.dim == 2:
  r2 = Expression("x[0]", degree=1)
  p = Expression("2.0*pi*x[0]", degree=1)
else:
  r2 = Expression("x[0]*x[0]", degree=1)
  p = Expression("4.0*pi*x[0]*x[0]", degree=1)

aF = dt*r2*DFe * inner(nabla_grad(F), nabla_grad(vF))*dx
U1 = r2*F*vF*dx - r2*F0*vF*dx
L1 = U1 + aF
J1 = derivative(L1, F, dF)

phiK = -p*dot(nabla_grad(F), n)*ds(1)

solve(L1 == 0.0, F, bcs1, J=J1)

# set up plot
y = iron.dolf2nump(F, mesh, dims=1)
fig, ax1 = plt.subplots()
ax1.set_ylim([0, 1.1*Fe0])
p1, = ax1.plot(coords, y[:, 0], color="#4daf4a", linewidth=2)
p3, = ax1.plot(coords, target, color="#000000", linewidth=1)
plt.show(block=False)
plt.pause(0.01)

# calculate actual uptake
old_flux = assemble(phiK)
dIk = Ik_max - Ik
new_flux = old_flux*dIk/(KIk+dIk)
#new_flux_p = new_flux/(4*math.pi*iron.rB**2)
new_flux_p = old_flux/(4*math.pi*iron.rB**2)
print((assemble(dot(grad(F),n)*ds(1)),new_flux_p))

# adjust problem and solve

g = Expression("PhiK", PhiK=new_flux_p, degree=1)

#aE = dt*r2*DFe * inner(nabla_grad(E), nabla_grad(vE))*dx
#U2 = dt*r2*E*vE*dx

L2 = L1 + dt*r2*g*vF*ds(1)
J2 = derivative(L2, F, dF)

#E.interpolate(Fe)
F.interpolate(zero)
solve(L2 == 0.0, F, bcs2, J=J2)

z = iron.dolf2nump(F, mesh, dims=1)
p2, = ax1.plot(coords, z[:, 0], color="#abccff", linewidth=2)
plt.draw()
plt.pause(0.001)

print((assemble(phiK),y[0,0],z[0,0]))

input('Enter to quit.')

quit()


#y = iron.dolf2nump(F, mesh, dims=1)

##############################################################################
# Create nonlinear problem and Newton solver

t = 0.0
next_print = args.reportTime
total_iron = 0.0

while (t < T):
  t += abs(fdt)

  F0.assign(F)
  #F0.interpolate(zero)
  F.interpolate(zero)

  # get flux without applying condition
  solve(L1 == 0.0, F, bcs1, J=J1)

  y = iron.dolf2nump(F, mesh, dims=1)
  p1.set_data(coords, y[:, 0])
  plt.draw()
  plt.pause(0.0001)

  # calculate actual uptake
  old_flux = assemble(phiK)

  dIk = Ik_max - Ik
  new_flux = old_flux*dIk/(KIk+dIk)
  new_flux_p = new_flux/(4*math.pi*iron.rB**2)

  # adjust problem and solve
  #LI = dt*r2*Expression("PhiK", PhiK=new_flux, degree=1)*vF*ds(1)
  #L2 = (U1-U0) + (aF-LI)
  #solve(L2 == 0.0, F, bcs2)

  #y = iron.dolf2nump(F, mesh, dims=1)
  #p2.set_data(coords, y[:, 0])
  #plt.draw()
  #plt.pause(0.01)

  #final_flux = assemble(phiK)
  #Ik = Ik + final_flux

  print("%g %g %g %g" % (t, Ik, final_flux, y[1]))

  if fdt < 0.0:
    fdt = fdt * 1.2
    if abs(fdt) > args.maxdt:
      fdt = args.maxdt
      dt.dt = fdt
    else:
      dt.dt = -fdt


