#!/bin/bash

control_c() {
  echo -en "\n*** Ouch! Exiting ***\n"
  exit $?
}
trap control_c SIGINT

prod=1e-9
#aggk=$2

for k in `seq 10 12`
do
  aggk="1e${k}"
  cat distances.txt | while read one dist
  do
    >&2 echo "k = $aggk, d = $dist"
    #echo "# d = $dist"
    ./secretors_time.py --mesh meshes/mesh_${one}.xml.gz --dist $dist \
      --aggk $aggk --maxTime inf --maxIron 1e6 \
      --dt -0.01 --reportTime inf --verbose 1 --prod $prod
  done
done
