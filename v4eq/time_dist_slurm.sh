#!/bin/bash

control_c() {
  echo -en "\n*** Ouch! Exiting ***\n"
  exit $?
}
trap control_c SIGINT

prod=1e-9
#aggk=$2

#aggk=1e9
#one=7
#dist=1e-5

#sbatch -p newnodes -t 4 -o data/coop-${aggk}-${dist}.out -e data/coop-${aggk}-${dist}.err \
#  --wrap "./secretors_time_4eq.py --mesh meshes/mesh_${one}.xml.gz --dist $dist --aggk $aggk --maxTime 10 --maxIron 1e6 --dt 1 --reportTime 0.01 --prod $prod --output data/coop-${aggk}-${dist}.txt"
#sbatch -p newnodes -t 4 -o data/cheat-${aggk}-${dist}.out -e data/cheat-${aggk}-${dist}.err \
#  --wrap "./secretors_time_4eq.py --mesh meshes/mesh_${one}.xml.gz --dist $dist --aggk $aggk --maxTime 10 --maxIron 1e6 --dt 1 --reportTime 0.01 --prod $prod --cheater --output data/cheat-${aggk}-${dist}.txt"

#exit

#for k in `seq 0 12`
#do
k=1
  aggk="1e${k}"
  cat distances.txt | while read one dist
  do
    echo "k = $aggk, d = $dist"
    sbatch -J fenics -p newnodes -t 12:00:00 -o data/coop-${aggk}-${dist}.out -e data/coop-${aggk}-${dist}.err \
      --wrap "./secretors_time_4eq.py --mesh meshes/mesh_${one}.xml.gz --dist $dist --aggk $aggk --maxTime inf --maxIron 1e6 --dt -0.01 --reportTime 0.01 --prod $prod --output data/coop-${aggk}-${dist}.txt"
    sbatch -J fenics -p newnodes -t 12:00:00 -o data/cheat-${aggk}-${dist}.out -e data/cheat-${aggk}-${dist}.err \
      --wrap "./secretors_time_4eq.py --mesh meshes/mesh_${one}.xml.gz --dist $dist --aggk $aggk --maxTime inf --maxIron 1e6 --dt -0.01 --reportTime 0.01 --prod $prod --cheater --output data/cheat-${aggk}-${dist}.txt"
  done
#done
