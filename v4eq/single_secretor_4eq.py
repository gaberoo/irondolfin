#!/usr/bin/env python3

import os
import sys
import signal
import argparse
import math
import numpy

os.environ['CC'] = 'gcc-8'
os.environ['CXX'] = 'g++-8'

##############################################################################
# Set signal handler

def signal_handler(signal, frame):
  print("Exiting...")
  sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)

##############################################################################
# FEniCS libraries

from dolfin import *
import mshr

import iron
args = iron.parser.parse_args()

##############################################################################
# Numpy/Matplotlib libraries

if args.plot:
  import ironPlot
  import matplotlib as mpl
  mpl.rcParams['toolbar'] = 'None'

##############################################################################
# Form compiler options

parameters["form_compiler"]["optimize"]     = True
parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["representation"] = "uflacs"
set_log_level(LogLevel.WARNING)

##############################################################################
# Some parameters

fdt  = args.dt
T    = args.maxTime         # total simulation time
aggk = Constant(args.aggk)  # iron aggregate size
Fe0  = 1e-7

#Rstar, fDFe = iron.R_star(args.prod, aggk, Fe0)
Rstar, fDFe = iron.R_star_2(args.prod, aggk, args.kappaFX, args.kappaEY)

if fdt < 0.0:
  sys.stderr.write("Increasing dt selected.\n")
  dt = Expression("dt", dt=-fdt, degree=1)
else:
  dt = Expression("dt", dt=fdt, degree=1)

xmax = args.xmax
eps  = 1e-7
rad  = iron.rB

##############################################################################
# make the 1D mesh

if args.verbose > 0:
  sys.stderr.write("Generating mesh...")
mesh = IntervalMesh(args.nmesh, iron.rB, xmax)
coords = iron.expand(mesh.coordinates(), iron.rB, xmax, args.scale)
mesh.coordinates()[:] = coords
if args.verbose > 0:
  sys.stderr.write("done. Minimum distance = %g.\n" % \
                   (coords[1]-coords[0]))

##############################################################################
# Define the function space

#V = FunctionSpace(mesh, "CG", 1)  # CG = Lagrange
P1 = FiniteElement("CG", interval, 1)
element = MixedElement([ P1, P1, P1, P1 ])
W = FunctionSpace(mesh, element)

##############################################################################
# Define boundaries

Fe = Constant(Fe0)
zero = Constant(0.0)

boundaries = MeshFunction("size_t", mesh, 0)
boundaries.set_all(0)

bndry_cell = CompiledSubDomain("on_boundary && x[0] == rB", rB=iron.rB)
bndry_out  = CompiledSubDomain("on_boundary && x[0] == xmax", xmax=xmax)

bndry_cell.mark(boundaries, 1)
bndry_out.mark(boundaries, 2)

bc_out_F  = DirichletBC(W.sub(0), Fe,    bndry_out)
bc_out_X  = DirichletBC(W.sub(1), zero,  bndry_out)
bc_out_Y  = DirichletBC(W.sub(2), zero,  bndry_out)
bc_cell_Y = DirichletBC(W.sub(2), zero,  bndry_cell)
bc_out_E  = DirichletBC(W.sub(3), zero,  bndry_out)

bcs = [ bc_out_F, bc_out_X, bc_out_Y, bc_cell_Y, bc_out_E ]

##############################################################################
# Setup measure for the boundaries (needed to calculate flux)

ds = Measure('ds', domain=mesh, subdomain_data=boundaries)

##############################################################################
# Define variational problem

# Define trial and test functions
du = TrialFunction(W)
u  = Function(W)
u0 = Function(W)
vF, vX, vY, vE = TestFunctions(W)

# Define functions

# Split mixed functions
F,  X,  Y,  E  = split(u)
F0, X0, Y0, E0 = split(u0)

# Create intial conditions and interpolate
u_init = Expression(("Fe-eps", "eps", "eps", "eps"),
                           Fe=Fe0, eps=0.0, degree=1)
u.interpolate(u_init)
u0.interpolate(u_init)

f = Constant(0.0)
kappaFX = Expression("kappaFX", kappaFX=args.kappaFX, degree=1)
kappaEY = Expression("kappaEY", kappaEY=args.kappaEY, degree=1)

gX = Constant(args.prod)
DFe = Expression("kB*Temp/(6.*pi*h*pow(aggk,1./3.)*rFe)",
                 element=P1, kB=iron.kB, Temp=iron.Temp,
                 h=iron.h, aggk=aggk, rFe=iron.rFe)

FourK = Expression("(aggk > 64) ? 4.0*pow(aggk,-1./3.) : 1.0",
                   aggk=aggk, degree=1)

fXY = kappaFX*X*iron.Max(FourK*F-E,0.0)
fEY = kappaEY*E

n = FacetNormal(mesh)

if args.dim == 2:
  r2 = Expression("x[0]", degree=1)
  p = Expression("2.0*pi*x[0]", degree=1)
else:
  r2 = Expression("x[0]*x[0]", degree=1)
  p = Expression("4.0*pi*x[0]*x[0]", degree=1)

aF = dt*r2*DFe       * inner(nabla_grad(F), nabla_grad(vF)) * dx
aX = dt*r2*iron.DSid * inner(nabla_grad(X), nabla_grad(vX)) * dx
aY = dt*r2*iron.DSid * inner(nabla_grad(Y), nabla_grad(vY)) * dx
aE = dt*r2*DFe       * inner(nabla_grad(E), nabla_grad(vE)) * dx
a0 = aF + aX + aY + aE

Lg = dt*r2*gX*vX*ds
L0 = - dt*r2*fXY/FourK*vF*dx \
     - dt*r2*fXY*vX*dx \
     + dt*r2*fEY*vY*dx \
     + dt*r2*(fXY-fEY)*vE*dx

U1 = r2* F*vF*dx + r2* X*vX*dx + r2* Y*vY*dx + r2* E*vE*dx
U0 = r2*F0*vF*dx + r2*X0*vX*dx + r2*Y0*vY*dx + r2*E0*vE*dx

L = (U1-U0) + (a0-L0-Lg)
dL = derivative(L, u, du)

n = FacetNormal(mesh)
p = Expression("4.0*pi*x[0]*x[0]", element=P1)
flux = -p*dot(nabla_grad(Y), n)*ds(1)

##############################################################################
# Create nonlinear problem and Newton solver

problem = iron.SecretorsEquation(dL, L, bcs)
solver = NewtonSolver()
solver.parameters["linear_solver"] = "lu"
solver.parameters["convergence_criterion"] = "incremental"
solver.parameters["relative_tolerance"] = 1e-10

t = 0.0
total_iron = 0.0

if args.output != None:
  #u_out = File(args.output)
  numpy.savetxt("%s_coords.txt" % args.output, coords[:, 0])
  F_out = open("%s_F.txt" % args.output, "w")
  X_out = open("%s_X.txt" % args.output, "w")
  Y_out = open("%s_Y.txt" % args.output, "w")
  E_out = open("%s_E.txt" % args.output, "w")
  time_out = open("%s_time.txt" % args.output, "w")
  iron_out = open("%s_iron.txt" % args.output, "w")

if args.plot:
  maxSid = args.prod*iron.rB**2/(iron.DSid*iron.rB)
  log_coords = [ math.log10(z) for z in coords ]
  y = iron.dolf2nump(u, mesh, dims=4)
  E0 = 4*args.aggk**(-1./3.)*Fe0
  lims = [ (0, 1.1*Fe0),
           (0, 1.1*maxSid),
           (0, 1e-12),
           (0, 1.1*E0) ]
  pvals = [ args.prod*iron.rB**2/iron.DSid/r for r in coords ]
  p1, p2, p3, p4, time \
          = ironPlot.plot_1D(log_coords, y, lims=lims,
                             pvals=pvals, Rstar=Rstar)
  #ax4.axhline(E0, color="grey")
  #Xr = [ args.prod*iron.rB**2/(iron.DSid*rr) for rr in coords ]
  #Estar = [ 1.0/(1.0+args.kappaEY/(args.kappaFX*a)) for a in Xr ]
  #print Estar
  #ax4.plot(log_coords, Estar, color="#cccccc", linewidth=2)
  input('Press enter to start.')

t = 0.0
next_print = args.reportTime
total_iron = 0.0

while (t < T):
  t += abs(fdt)

  u0.assign(u)
  solver.solve(problem, u.vector())

  total_flux = assemble(flux) * iron.Na*iron.DSid
  total_iron += abs(fdt)*float(total_flux)

  if fdt < 0.0:
    fdt = fdt * 1.2
    if abs(fdt) > args.maxdt:
      fdt = args.maxdt
      dt.dt = fdt
    else:
      dt.dt = -fdt

  if args.verbose > 1:
    sys.stderr.write("New dt = %g\n" % fdt)

  if args.plot or args.output != None:
    y = iron.dolf2nump(u, mesh, dims=4)

  if args.output != None:
    iron.write(F_out, y[:, 0], time=None)
    iron.write(X_out, y[:, 1], time=None)
    iron.write(Y_out, y[:, 2], time=None)
    iron.write(E_out, y[:, 3], time=None)
    time_out.write("%g\n" % t)
    iron_out.write("%g %g\n" % (total_flux, total_iron))

  if args.plot:
    ironPlot.plot_1D(log_coords, y, (p1, p2, p3, p4, time), new_time=t)

  if args.verbose > 0:
    sys.stderr.write("Flux (t=%12f) = %g\r" % (t, total_flux))

  if t >= next_print:
    next_print += args.reportTime
    print("%g %g %g %g %g %g" % (
          t, total_flux, total_iron, args.aggk, args.prod, args.kappaEY))

if args.output != None:
  F_out.close()
  X_out.close()
  Y_out.close()
  E_out.close()
  time_out.close()
  iron_out.close()

if args.plot:
  input('Enter to quit.')

