#!/bin/bash

control_c() {
  echo -en "\n*** Ouch! Exiting ***\n"
  exit $?
}
trap control_c SIGINT

for prod in $( cat prod_rates.txt ); do
  for aggk in $( cat aggk.txt ); do
    echo "$prod $aggk"
  done
done | \
  parallel -j 12 --colsep ' ' \
    ./single_uptake_time_4eq.py --dt -0.01 --aggk {2} \
      --maxdt 1e6 --nmesh 400 --xmax 10.0 --prod {1} \
      --kappaEY 1e-6
