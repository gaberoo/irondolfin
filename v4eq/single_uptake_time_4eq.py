#!/usr/bin/env python3

import sys
import signal
import argparse
import math

##############################################################################
# Set signal handler

def signal_handler(signal, frame):
  print("Exiting...")
  sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)

##############################################################################
# FEniCS libraries

import mshr
from dolfin import *

import iron
import numpy as np

args = iron.parser.parse_args()

##############################################################################
# Numpy/Matplotlib libraries

if args.plot:
  import ironPlot
  import matplotlib as mpl
  import matplotlib.pyplot as plt
  mpl.rcParams['toolbar'] = 'None'

##############################################################################
# Form compiler options

parameters["form_compiler"]["optimize"]     = True
parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["representation"] = "uflacs"

set_log_level(LogLevel.WARNING)

##############################################################################
# Some parameters

dt   = Constant(args.dt)       # time step
aggk = Constant(args.aggk)     # iron aggregate size
fDFe  = iron.Diff(aggk**(1./3.)*iron.rFe) # diffusion coefficient of iron
fdt  = args.dt
Fe0  = args.rho

xmax = args.xmax
eps  = 1e-7
rad  = iron.rB
crit_iron = 1e6
Rstar = args.prod*iron.rB**2/(fDFe*Fe0)

##############################################################################
# make the 1D mesh

if args.verbose > 1:
  sys.stderr.write("Generating mesh...")
mesh = IntervalMesh(args.nmesh, iron.rB, xmax)
coords = iron.expand(mesh.coordinates(), iron.rB, xmax, args.scale)
mesh.coordinates()[:] = coords
if args.verbose > 1:
  sys.stderr.write(" minimal distance = %g\n" % ( coords[1]-coords[0] ))

##############################################################################
# Define the function space

#V = FunctionSpace(mesh, "CG", 1)  # CG = Lagrange
#W = MixedFunctionSpace([V,V,V])
P1 = FiniteElement("CG", interval, 1)
element = MixedElement([P1, P1, P1, P1])
W = FunctionSpace(mesh, element)

##############################################################################
# Set the time step

if fdt < 0.0:
  if args.verbose: sys.stderr.write("Increasing dt selected.\n")
  dt = Expression("dt", dt=-fdt, element=P1)
else:
  dt = Expression("dt", dt=fdt, element=P1)

##############################################################################
# Define boundaries

Fe = Constant(Fe0)
zero = Constant(0.0)

boundaries = MeshFunction("size_t", mesh, 0)
boundaries.set_all(0)

bndry_cell = CompiledSubDomain("on_boundary && x[0] == rB", rB=iron.rB)
bndry_out  = CompiledSubDomain("on_boundary && x[0] == xmax", xmax=xmax)

bndry_cell.mark(boundaries, 1)
bndry_out.mark(boundaries, 2)

bc_out_F  = DirichletBC(W.sub(0), Fe,    bndry_out)
bc_out_X  = DirichletBC(W.sub(1), zero,  bndry_out)
bc_out_Y  = DirichletBC(W.sub(2), zero,  bndry_out)
bc_cell_Y = DirichletBC(W.sub(2), zero,  bndry_cell)
bc_out_E  = DirichletBC(W.sub(3), zero,  bndry_out)

bcs = [ bc_out_F, bc_out_X, bc_out_Y, bc_cell_Y, bc_out_E ]

##############################################################################
# Setup measure for the boundaries (needed to calculate flux)

ds = Measure('ds', domain=mesh, subdomain_data=boundaries)

##############################################################################
# Define variational problem

# Define trial and test functions
du = TrialFunction(W)
u  = Function(W)
u0 = Function(W)
vF, vX, vY, vE = TestFunctions(W)

# Define functions

# Split mixed functions
F,  X,  Y,  E  = split(u)
F0, X0, Y0, E0 = split(u0)

# Create intial conditions and interpolate
#u_init = InitialConditions(Fe0,element=P1)
u_init = Expression(("Fe-eps", "eps", "eps", "eps"), \
                           Fe=Fe0, eps=0.0, degree=1)
u.interpolate(u_init)
u0.interpolate(u_init)

f = Constant(0.0)
kappaFX = Expression("kappaFX", kappaFX=args.kappaFX, degree=1)
kappaEY = Expression("kappaEY", kappaEY=args.kappaEY, degree=1)

gX  = Constant(args.prod)
DFe = Expression("kB*Temp/(6.*pi*h*pow(aggk,1./3.)*rFe)",
                        element=P1, kB=iron.kB, Temp=iron.Temp,
                        h=iron.h, aggk=aggk, rFe=iron.rFe)

r2  = Expression("x[0]*x[0]", element=P1)
p = Expression("4.0*pi*x[0]*x[0]", degree=1)

# fXY = kappaFX*X*F

FourK = Expression("(aggk > 64) ? 4.0*pow(aggk,-1./3.) : 1.0",
                          aggk=aggk, degree=1)

fXY = kappaFX*X*iron.Max(FourK*F-E,0.0)
fEY = kappaEY*E

aF = dt*r2*DFe *inner(nabla_grad(F),
                             nabla_grad(vF)) * dx
aX = dt*r2*iron.DSid*inner(nabla_grad(X),
                             nabla_grad(vX)) * dx
aY = dt*r2*iron.DSid*inner(nabla_grad(Y),
                             nabla_grad(vY)) * dx
aE = dt*r2*DFe *inner(nabla_grad(E),
                             nabla_grad(vE)) * dx
a0 = aF + aX + aY + aE

Lg = dt*r2*gX*vX*ds
L0 = - dt*r2*fXY*vF*dx \
     - dt*r2*fXY*vX*dx \
     + dt*r2*fEY*vY*dx \
     + dt*r2*(fXY-fEY)*vE*dx

U1 = r2* F*vF*dx + r2* X*vX*dx + r2* Y*vY*dx + r2* E*vE*dx
U0 = r2*F0*vF*dx + r2*X0*vX*dx + r2*Y0*vY*dx + r2*E0*vE*dx

L = (U1-U0) + (a0-L0-Lg)
dL = derivative(L, u, du)

n = FacetNormal(mesh)
flux = -p*dot(nabla_grad(Y), n)*ds(1)

##############################################################################
# Create nonlinear problem and Newton solver

problem = iron.SecretorsEquation(dL, L, bcs)
solver = NewtonSolver()
solver.parameters["linear_solver"] = "lu"
solver.parameters["convergence_criterion"] = "incremental"
solver.parameters["relative_tolerance"] = 1e-10

t = 0.0
total_iron = 0.0
maxE = 4.*args.aggk**(-1./3.)*1e-7

if args.plot:
  log_coords = [ math.log10(z) for z in coords ]
  y = iron.dolf2nump(u, mesh, dims=4)
  fig, (ax1, ax2, ax3, ax4) = plt.subplots(4, sharex=True)
  ax1.set_ylim([0, 1.1e-7])
  ax2.set_ylim([0, args.prod**0.55])
  ax3.set_ylim([0, 1e-9]) #args.prod**1.1])
  ax4.set_ylim([0, 1.1*maxE])
  ax1.axvline(math.log10(Rstar), color="grey")
  ax2.axvline(math.log10(Rstar), color="grey")
  ax3.axvline(math.log10(Rstar), color="grey")
  ax4.axvline(math.log10(Rstar), color="grey")
  ax4.axhline(maxE, color="grey")
  p1, = ax1.plot(log_coords, y[:, 0], color="#4daf4a", linewidth=2.0)
  p2, = ax2.plot(log_coords, y[:, 1], color="#ff7f00", linewidth=2.0)
  p3, = ax3.plot(log_coords, y[:, 2], color="#984ea3", linewidth=2.0)
  p4, = ax4.plot(log_coords, y[:, 3], color="#000000", linewidth=2.0)
  plt.show(block=False)
  plt.pause(0.01)

final_time = float("inf")
while total_iron < 1e6:
  t += abs(fdt)
  u0.assign(u)

  solver.solve(problem, u.vector())

  total_flux = assemble(flux) * iron.Na*iron.DSid
  new_total_iron = total_iron + abs(fdt)*total_flux

  if new_total_iron > crit_iron:
    diff = (crit_iron-total_iron)/(new_total_iron-total_iron)
    final_time = (t-abs(fdt)) + diff*abs(fdt)
    total_iron = new_total_iron
    break
  else:
    total_iron = new_total_iron
    if args.verbose > 1:
      sys.stderr.write("t = %g, flux = %g, iron = %g (%5.2f%%)         \r" \
                       % (t,assemble(flux),total_iron,100*total_iron/crit_iron))
    if args.plot:
      y = iron.dolf2nump(u, mesh, dims=4)
      p1.set_data(log_coords, y[:, 0])
      p2.set_data(log_coords, y[:, 1])
      p3.set_data(log_coords, y[:, 2])
      p4.set_data(log_coords, y[:, 3])
      plt.draw()

      plt.pause(0.01)
  if fdt < 0.0:
    fdt = fdt * 1.2
    if abs(fdt) > args.maxdt:
      fdt = args.maxdt
      dt.dt = fdt
    else:
      dt.dt = -fdt
    if args.verbose > 2:
      sys.stderr.write("New dt = %g\n" % fdt)

if args.verbose > 1:
  sys.stderr.write("Done.                                          \n")

ktime = iron.KeeperTime(float(aggk), Fe0=Fe0)
print(args.nmesh, args.prod, float(aggk), fdt, final_time, args.kappaEY, ktime)

if args.plot:
  print("Final time = %f" % final_time)
  input('Enter to continue.')

