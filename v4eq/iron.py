#from mshr import *
#from dolfin import *
import mshr
import dolfin

import math
import numpy as np
import scipy.optimize as opt

import sys
import argparse

# Some constants
kB = 1.38e-23                     # Boltzman's constant
Temp = 293                        # Temperature
h = 1.003e-3                      # Viscosity
Na = 6.023e23                     # Avogadro's number
rB = 1e-6                         # Bacterial radius [m]
rFe = 1e-10                       # Iron iron radius [m]
rSid = 1e-9                       # Siderophores radius [m]
Fe0 = 1e-7                        # Background concentration of iron
SidLoss = 1                       # Siderophore loss rate
SidMax = 1e-4
kappa = 1e6                       # Siderophore-Iron binding affinity
max_uptake = 500                  # Maximum uptake rate
SidProd = 1e-9

def Max(a, b): return (a+b+abs(a-b))/dolfin.Constant(2)
def Min(a, b): return (a+b-abs(a-b))/dolfin.Constant(2)

##############################################################################
# Calculate the diffusion coefficient

def Diff(r):
  return kB*Temp/(6.0*math.pi*h*r)

DSid = kB*Temp/(6.0*math.pi*h*rSid)

def R_star(prod, aggk, F=1e-7):
  fDFe = Diff(aggk**(1./3.)*rFe)
  RStar = prod*rB**2/(fDFe*F)
  return RStar, fDFe

def R_star_2(prod, aggk, kappaX, kappaL):
  fDFe = Diff(rSid)
  RStar = prod*rB**2/fDFe * kappaX/kappaL
  return RStar, fDFe

##############################################################################
# Keeper solutions

def KeeperFlux(aggk, Fe0):
  fDFe = Diff(aggk**(1./3.)*rFe)
  return Fe0*4*math.pi*rB*fDFe

def KeeperIron(time, aggk, Fe0=1e-7, kappaEY=1e-6):
  kJk = KeeperFlux(aggk, Fe0)*Na
  Psik = 4./3.*kappaEY*aggk**(-1./3.)
  return kJk*(time + (math.exp(-Psik*time) - 1.0)/Psik)

def KeeperTime(aggk, maxIron=1e6, Fe0=1e-7, kappaEY=1e-6):
  f = lambda x: (KeeperIron(x, aggk, Fe0, kappaEY) - maxIron)
  time = opt.brentq(f=f, a=0.0, b=1e10)
  return time

##############################################################################
# Various Dolfin functions

def dolf2nump(u, mesh, dims=4):
  v = u.compute_vertex_values(mesh) #.vector().array()
  n = int(v.shape[0]/dims)
  w = v.reshape((dims, n)).transpose()
  return w

##############################################################################
# Write to file

def write(out, arr, time=0.0, log10=False):
  if time != None:
    out.write("%.12g " % time)

  for x in arr:
    if log10:
      if x > 0.0:
        out.write("%.12g " % math.log10(x))
      else:
        out.write("-Inf ")
    else:
      out.write("%.12g " % x)

  out.write("\n")
  out.flush()

##############################################################################
# Make a mesh

class CellDomain(dolfin.SubDomain):
  def __init__(self, center, radius, alpha=1.5):
    dolfin.SubDomain.__init__(self)
    self.p = center
    self.rad = radius
    self.alpha = alpha
  def inside(self, x, on_boundary):
    r = math.sqrt((x[0]-self.p[0])**2 + (x[1]-self.p[1])**2)
    return r < self.alpha*self.rad

#class SquareDomain(dolfin.SubDomain):
#  def __init__(self, left, right):
#    dolfin.SubDomain.__init__(self)
#    self.left = left
#    self.right = right
#  def inside(self, x, on_boundary):
#    dx = x[0]-p1[0]
#    dy = x[1]-p1[1]
#    return dx < 2*rad and dy < 2*rad

def cart2pol(x):
  rho = np.sqrt(x[0]**2 + x[1]**2)
  phi = np.arctan2(x[1], x[0])
  return rho, phi

def pol2cart(q):
  x = q[0]*np.cos(q[1])
  y = q[0]*np.sin(q[1])
  return [ x, y ]

def rexpand(x, s, rmin=1.0, expand_type="pow"):
  rho, phi = cart2pol(x)
  if rho > rmin:
    if expand_type == "lin":
      r1 = s*rho
    elif expand_type == "exp":
      r1 = rho * s**(rho-rmin)
    else:
      r1 = rmin*(rho/rmin)**s
    return pol2cart([r1, phi])
  else:
    return x

def expand(x, a, b, s):
  x1 = x
  for i, xi in enumerate(x):
    sgn = np.sign(xi)
    z = abs(xi)
    if z > a:
      x1[i] = sgn*(a+(b-a)*((z-a)/(b-a))**s)
      #x1[i] = sgn * z**s
      #x1[i]  = xi*s
  return x1

def xyexpand(x, a, b, s):
  y   = [ abs(z) for z in x ]
  sgn = [ np.sign(z) for z in x ]
  if y[0] > a[0] and y[1] > a[1]:
    x0 = sgn[0]*(a[0]+(b[0]-a[0])*((y[0]-a[0])/(b[0]-a[0]))**s)
    x1 = sgn[1]*(a[1]+(b[1]-a[1])*((y[1]-a[1])/(b[1]-a[1]))**s)
    #x0 = sgn[0]*(a[0]+(y[0]-a[0])*s)
    #x1 = sgn[1]*(a[1]+(y[1]-a[1])*s)
    return [ x0, x1 ]
  else:
    return x

##############################################################################

def make_mesh(distance, xmax, ymax, init_size,
              nrefine=3, rad=rB, rect=False,
              factor=None, expandMin=1e-5):
  "Creates a mesh with two cells in it"

  p1 = dolfin.Point( distance/2.0, 0.0)
  p2 = dolfin.Point(-distance/2.0, 0.0)

  domain = None
  if rect:
    left  = dolfin.Point(-xmax, -ymax)
    right = dolfin.Point( xmax, ymax)
    domain = mshr.Rectangle(left, right) - \
             mshr.Circle(p1, rad) - mshr.Circle(p2, rad)
  else:
    if distance > 0.0:
      domain = mshr.Ellipse(dolfin.Point(0.0, 0.0), xmax, ymax) - \
               mshr.Circle(p1, rad) - mshr.Circle(p2, rad)
    else:
      domain = mshr.Ellipse(dolfin.Point(0.0, 0.0), xmax, ymax) - \
               mshr.Circle(p1,rad)

    # Create rectangular mesh
    mesh = mshr.generate_mesh(domain, init_size)

    if nrefine > 0:
      sys.stderr.write("Refining mesh...")
      for i in range(nrefine):
        # Mark cells for refinement
        markers = dolfin.MeshFunction("bool", mesh,
                                      mesh.topology().dim())
        markers.set_all(False)
        for cell in dolfin.cells(mesh):
          r1 = cell.midpoint().distance(p1)
          if distance > 0.0:
            r2 = cell.midpoint().distance(p2)
            if r1 < rad*(1.0+0.2/(i+1)) or r2 < rad*(1.0+0.2/(i+1)):
              markers[cell.index()] = True
            else:
              if r1 < rad*(1.0+0.2/(i+1)):
                markers[cell.index()] = True
            # Refine mesh
            mesh = dolfin.refine(mesh, markers)
            # Snap boundary
            bndry_cell_1 = dolfin.CellBoundary([p1], rad, 1e-7/(i+1.))
            mesh.snap_boundary(bndry_cell_1)
            if distance > 0.0:
              bndry_cell_2 = dolfin.CellBoundary([p2], rad, 1e-7/(i+1.))
              mesh.snap_boundary(bndry_cell_2)
        sys.stderr.write("done.\n")

    if factor != None and factor != 1.0:
      sys.stderr.write("Expanding coordinates...")
      coords = mesh.coordinates()
      new_coords = [ rexpand(xy, factor, expandMin, "pow") \
                     for xy in coords ]
      xy = np.array(new_coords)
      mesh.coordinates()[:] = xy
      xmax = max([ abs(z) for z in xy[:, 0] ])
      ymax = max([ abs(z) for z in xy[:, 1] ])
      bndry_out = dolfin.DomainBoundary([p1, p2], rad, xmax, rB/2.0)
      mesh.snap_boundary(bndry_out)
      sys.stderr.write("done. Max = (%f,%f)\n" % (xmax, ymax))

  return mesh, xmax, ymax

##############################################################################


##############################################################################
# Define boundaries

#class Hole(SubDomain):
#    def __init__(self,center,radius):
#        SubDomain.__init__(self)
#        self.center = center
#        self.radius = radius
#    def inside(self, x, on_boundary):
#        r = sqrt((x[0]-self.center[0])**2 + (x[1]-self.center[0])**2)
#        return r < 1.5*self.radius # slightly larger
#    def snap(self, x):
#        r = sqrt((x[0]-self.center[0])**2 + (x[1]-self.center[1])**2)
#        if r < 1.5*self.radius:
#            x[0] = self.center[0] + (self.radius/r)*(x[0]-self.center[0])
#            x[1] = self.center[1] + (self.radius/r)*(x[1]-self.center[1])

# ----------------------------------------------------------------------------

def inner_boundary(x, cells, radius, eps):
  rad = [ math.sqrt( (x[0]-p[0])**2 + (x[1]-p[1])**2 ) for p in cells ]
  on_b = [ (abs(r-radius) < eps) for r in rad ]
  return any(on_b)

# ----------------------------------------------------------------------------

class IronCellBoundary(dolfin.SubDomain):
  def __init__(self, cells, radius, eps):
    dolfin.SubDomain.__init__(self)
    self.cells = cells 
    self.radius = radius
    self.eps = eps
  def inside(self, x, on_boundary):
    return on_boundary and \
           inner_boundary(x, self.cells, self.radius, self.eps)
  def snap(self, x):
    for p in self.cells:
      rad = math.sqrt( (x[0]-p[0])**2 + (x[1]-p[1])**2 )
      on_b = (abs(rad-self.radius) < self.eps)
      if on_b:
        x[0] = p[0] + (self.radius/rad)*(x[0]-p[0])
        x[1] = p[1] + (self.radius/rad)*(x[1]-p[1])

# ----------------------------------------------------------------------------

class DomainBoundary(dolfin.SubDomain):
  def __init__(self, cells, radius, max_rad, eps):
    dolfin.SubDomain.__init__(self)
    self.cells = cells
    self.radius = radius
    self.eps = eps
    self.max_rad = max_rad
  def inside(self, x, on_boundary):
    return on_boundary and \
           not inner_boundary(x, self.cells, self.radius, self.eps)
  def snap(self, x):
    r = math.sqrt(x[0]**2+x[1]**2)
    if r > self.max_rad*.5:
      x[0] = (self.max_rad/r)*x[0]
      x[1] = (self.max_rad/r)*x[1]

##############################################################################
# Class representing the intial conditions

class InitialConditions(dolfin.Expression):
  def __init__(self, Fe, degree=1, eps=0.0, *kwargs):
    dolfin.Expression.__init__(self, degree=degree)
    self.Fe = Fe
    self.eps = eps
  def eval(self, values, x):
    values[0] = self.Fe-self.eps
    values[1] = self.eps
    values[2] = self.eps
    values[3] = self.eps
  def value_shape(self):
    return (4,)

##############################################################################
# Class for interfacing with the Newton solver

class SecretorsEquation(dolfin.NonlinearProblem):
  def __init__(self, a, L, bcs):
    dolfin.NonlinearProblem.__init__(self)
    self.L = L
    self.a = a
    self.bcs = bcs
  def F(self, b, x):
    dolfin.assemble(self.L, tensor=b)
    for bc in self.bcs:
      bc.apply(b, x)
  def J(self, A, x):
    dolfin.assemble(self.a, tensor=A)
    for bc in self.bcs:
      bc.apply(A)

##############################################################################
# Class single cell

parser = argparse.ArgumentParser(description='Iron diffusion with two cells.')
parser.add_argument('--nmesh', metavar='points', type=int, default=100,
                    help="Number of mesh points")
parser.add_argument('--scale', metavar='value', type=float, default=4.0,
                    help="Mesh stretching parameter")
parser.add_argument('--aggk', metavar='k', type=float, default=1.0,
                    help='Aggregation')
parser.add_argument('--rho', metavar='rho', type=float, default=1e-7,
                    help='Background iron concentration')
parser.add_argument('--dt', metavar='time', type=float, default=1.0,
                    help='Time step')
parser.add_argument('--maxdt', metavar='time', type=float, default=1000.0,
                    help='Max time step')
parser.add_argument('--reportTime', metavar='time', type=float,
                    default=float("inf"), help='Reporting time step')
parser.add_argument('--maxTime', metavar='time', type=float,
                    default=1000.0, help='Max time')
parser.add_argument('--prod', metavar='rate', type=float, default=SidProd,
                    help='Siderophore production rate')
parser.add_argument('--xmax', metavar='pos', type=float, default=0.1,
                    help='Maximum radial distance')
parser.add_argument('--xview', metavar='len', type=float, default=None,
                    help='Viewport size')
parser.add_argument("--output", metavar="file", default=None,
                    help="Output filename")
parser.add_argument("--verbose", metavar="level", type=int, default=0,
                    help="Logging level")
parser.add_argument('--kappaFX', metavar='kappaFX', type=float, default=1e6,
                    help='Binding rate Sid-Fe')
parser.add_argument('--kappaEY', metavar='kappaEY', type=float, default=1e-6,
                    help='Surface dissolution rate')
parser.add_argument("--plot", action="store_true")
parser.add_argument("--dim", metavar='n', type=int, default=3,
                    help="dimensions")

