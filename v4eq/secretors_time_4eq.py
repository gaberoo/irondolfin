#!/usr/bin/env python3

import os
import sys
import signal
import argparse

from subprocess import call
from tempfile import mkstemp

os.environ['CC'] = 'gcc-8'
os.environ['CXX'] = 'g++-8'

##############################################################################
# Set signal handler

def signal_handler(signal, frame):
    print("Exiting...")
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

##############################################################################
# FEniCS libraries

from mshr import *
from dolfin import *
from iron import *

parser = argparse.ArgumentParser(description='Iron diffusion with two cells.')
parser.add_argument('--mesh', metavar='file', default=None,
                    help="Mesh file")
parser.add_argument('--nmesh', metavar='points', type=int, default=100,
                    help="Number of mesh points")
parser.add_argument('--scale', metavar='value', type=float, default=4.0,
                    help="Mesh stretching parameter")
parser.add_argument('--aggk', metavar='k', type=float, default=1.0,
                    help='Aggregation')
parser.add_argument('--dt', metavar='time', type=float, default=1.0,
                    help='Time step')
parser.add_argument('--maxdt', metavar='time', type=float, default=1000.0,
                    help='Max time step')
parser.add_argument('--reportTime', metavar='time', type=float, 
                    default=float("inf"), help='Reporting time step')
parser.add_argument('--maxTime', metavar='time', type=float, 
                    default=1000.0, help='Max time')
parser.add_argument('--maxIron', metavar='iron', type=float, 
                    default=1e6, help='Max iron')
parser.add_argument('--prod', metavar='rate', type=float, default=SidProd,
                    help='Siderophore production rate')
parser.add_argument('--xmax', metavar='pos', type=float, default=0.1,
                    help='Maximum radial distance')
parser.add_argument('--ymax', metavar='pos', type=float, default=0.1,
                    help='Maximum radial distance')
parser.add_argument('--xview', metavar='len', type=float, default=None,
                    help='Viewport size')
parser.add_argument('-d','--dist', metavar='len', type=float, default=1e-5,
                    help='Distance between two cells')
parser.add_argument('--meshSize', metavar='res', type=int, default=20,
                    help='Initial mesh resolution')
parser.add_argument('--expandMin', metavar='len', type=float, default=1e-5,
                    help='Grid expandsion above this lenth')
parser.add_argument('--factor', metavar='value', type=float, default=2.0,
                    help='Expansion factor')
parser.add_argument('--refine', metavar='num', type=int, default=0,
                    help='Number of refinement steps')
parser.add_argument("--output", metavar="file", default=None,
                    help="Output filename")
parser.add_argument("--verbose", metavar="level", type=int, default=0,
                    help="Logging level")
parser.add_argument("--dim", metavar='n', type=int, default=3,
                    help="dimensions")
parser.add_argument('--kappaFX', metavar='kappaFX', type=float, default=1e6,
                    help='Binding rate Sid-Fe')
parser.add_argument('--kappaEY', metavar='kappaEY', type=float, default=1e-6,
                    help='Surface dissolution rate')
parser.add_argument("--plot", action="store_true")
parser.add_argument("--cheater", action="store_true")
args = parser.parse_args()

if args.output:
  output = open(args.output, "w", 1) # turn on line buffering

##############################################################################
# Numpy/Matplotlib libraries

if args.plot:
  import numpy as np
  import matplotlib as mpl
  import matplotlib.pyplot as plt
  import ironPlot
  mpl.rcParams['toolbar'] = 'None'

##############################################################################
# Form compiler options

parameters["form_compiler"]["optimize"]     = True
parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["representation"] = "uflacs"

set_log_level(LogLevel.WARNING)

##############################################################################
# Some parameters

T    = args.maxTime            # total simulation time
fdt  = args.dt                 # time step
aggk = Constant(args.aggk)     # iron aggregate size
Fe0  = 1e-7

Rstar, fDFe = R_star(args.prod, aggk, Fe0)

if fdt < 0.0:
  if args.verbose: sys.stderr.write("Increasing dt selected.\n")
  dt = Expression("dt", dt=-fdt, degree=1)
else:
  dt = Expression("dt", dt=fdt, degree=1)

xmax = args.xmax
ymax = args.ymax
eps  = 1e-7
rad  = rB
Rstar = args.prod*rB**2/(fDFe*Fe0)

crit_iron = args.maxIron

distance  = args.dist          # distance between cells
mesh_file = args.mesh          # mesh file (must correspond to distance)

##############################################################################
# make the 2D mesh

mesh = None

p1 = Point(-distance/2.0, 0.0)
if distance > 0.0:
    p2 = Point(distance/2.0, 0.0)
    cells = [ p1, p2 ]
else:
    cells = [ p1 ]

if mesh_file == None:
    sys.stderr.write("No mesh file supplied. Exiting...")
    quit()

if args.verbose > 1:
    sys.stderr.write("Reading mesh...")

mesh = Mesh(mesh_file)
coords = mesh.coordinates()
rho = [ cart2pol(z)[0] for z in coords ]
xmax = max(rho)
ymax = max(rho)

if args.verbose > 1:
  sys.stderr.write("xmax = %g, ymax = %g.\n" % (xmax, ymax))

if args.plot:
    ironPlot.plot_mesh(mesh, pause=True, xlim=1e-5, ylim=1e-5)

boundaries = MeshFunction("size_t", mesh, 1)
boundaries.set_all(0)

#cpp_cell = """
#on_boundary && 
#  sqrt((fabs(x[0])-(dist/2.0))*(fabs(x[0])-(dist/2.0)) + x[1]*x[1]) < 2*rB
#"""
cpp_cell = "on_boundary && sqrt(x[0]*x[0] + x[1]*x[1]) < 0.5*xmax"
cpp_out  = "on_boundary && sqrt(x[0]*x[0] + x[1]*x[1]) > 0.9*xmax"

#bndry_out  = CompiledSubDomain(cpp_out, xmax=xmax)
#bndry_cell = CompiledSubDomain(cpp_cell, xmax=xmax)

bndry_out  = DomainBoundary(cells, rad, xmax, eps)
bndry_cell = IronCellBoundary(cells, rad, eps)

bndry_cell_1 = IronCellBoundary([p1], rad, eps)
bndry_cell_1.mark(boundaries, 1)

if distance > 0.0:
    bndry_cell_2 = IronCellBoundary([p2], rad, eps)
    bndry_cell_2.mark(boundaries, 2)

bndry_out.mark(boundaries, 3)

# plot(boundaries,interactive=True)

##############################################################################
# Define the function space

P1 = FiniteElement("CG", triangle, 1)
element = MixedElement([P1, P1, P1, P1])
W = FunctionSpace(mesh, element)

##############################################################################
# Define boundaries

Fe = Constant(Fe0)
zero = Constant(0.0)

bc_out_F  = DirichletBC(W.sub(0), Fe,    bndry_out)
bc_out_X  = DirichletBC(W.sub(1), zero,  bndry_out)
bc_out_Y  = DirichletBC(W.sub(2), zero,  bndry_out)
bc_cell_Y = DirichletBC(W.sub(2), zero,  bndry_cell)
bc_out_E  = DirichletBC(W.sub(3), zero,  bndry_out)

bcs = [ bc_out_F, bc_out_X, bc_out_Y, bc_cell_Y, bc_out_E ]

##############################################################################
# Setup measure for the boundaries (needed to calculate flux)

ds = Measure('ds', domain=mesh, subdomain_data=boundaries)

##############################################################################
# Define variational problem

# Define trial and test functions
du = TrialFunction(W)
u  = Function(W)
u0 = Function(W)
vF, vX, vY, vE = TestFunctions(W)

# Define functions

# Split mixed functions
F,  X,  Y,  E  = split(u)
F0, X0, Y0, E0 = split(u0)

f = Constant(0.0)
kappaFX = Expression("kappaFX", kappaFX=args.kappaFX, degree=1)
kappaEY = Expression("kappaEY", kappaEY=args.kappaEY, degree=1)

n = FacetNormal(mesh)
gX = Constant(args.prod)
DFe = Expression("kB*Temp/(6.*pi*h*pow(aggk,1./3.)*rFe)",
                 element=P1,
                 kB=kB,Temp=Temp,h=h,aggk=aggk,rFe=rFe)

FourK = Expression("(aggk > 64) ? 4.0*pow(aggk,-1./3.) : 1.0",
                   aggk=aggk, degree=1)

fXY = kappaFX*X*Max(FourK*F-E,0.0)
fEY = kappaEY*E

if args.dim == 2:
    p = Constant(2.0/3.0*rB)
    r = Constant(1.0)
else:
    p = Expression("pi*fabs(x[1])", element=P1)
    r = Expression("fabs(x[1])", element=P1)

######### TO HERE #########

# diffusion equations
aF = dt*r*DFe  * inner(nabla_grad(F), nabla_grad(vF)) * dx
aX = dt*r*DSid * inner(nabla_grad(X), nabla_grad(vX)) * dx
aY = dt*r*DSid * inner(nabla_grad(Y), nabla_grad(vY)) * dx
aE = dt*r*DFe  * inner(nabla_grad(E), nabla_grad(vE)) * dx
a0 = aF + aX + aY + aE

# production of siderophores
Lg1 = dt*r*gX*vX*ds(1)
Lg2 = dt*r*gX*vX*ds(2)

# reaction equations
L0 = - dt*r*fXY/FourK*vF*dx \
     - dt*r*fXY*vX*dx \
     + dt*r*fEY*vY*dx \
     + dt*r*(fXY-fEY)*vE*dx

# variational terms
U1 = r* F*vF*dx + r* X*vX*dx + r* Y*vY*dx + r* E*vE*dx
U0 = r*F0*vF*dx + r*X0*vX*dx + r*Y0*vY*dx + r*E0*vE*dx

if args.cheater or distance == 0.0:
    L = (U1-U0) + (a0-L0-Lg1)
else:
    L = (U1-U0) + (a0-L0-Lg1-Lg2)

dL = derivative(L, u, du)

flux1 = -p*dot(nabla_grad(Y),n)*ds(1)
flux2 = -p*dot(nabla_grad(Y),n)*ds(2)

##############################################################################
# Create nonlinear problem and Newton solver

problem = SecretorsEquation(dL,L,bcs)
solver = NewtonSolver()
solver.parameters["linear_solver"] = "lu"
solver.parameters["convergence_criterion"] = "incremental"
solver.parameters["relative_tolerance"] = 1e-6

# Create intial conditions and interpolate
#u_init = InitialConditions(Fe0)
u_init = Expression(("Fe-eps", "eps", "eps", "eps"), Fe=Fe0, eps=0.0, degree=1)
u.interpolate(u_init)
u0.interpolate(u_init)

if args.plot:
    maxSid = args.prod*rB**2/(DSid*rB)
    E0 = 4*args.aggk**(-1./3.)*Fe0
    func = interpolate(u,W)
    func.set_allow_extrapolation(True)
    ax_lims = [ (0, 1.1*Fe0),
                (0, 1.1*maxSid),
                (0, 1e-12),
                (0, 1.1*E0) ]
    #axf = plot_mesh_func(mesh,func,idx=2,xlim=xmax,ylim=ymax)
    ps = ironPlot.plot_mesh_func_lin(mesh,func,distance,
                                     xmax=xmax,idx=0,nsteps=1000,
                                     lims=ax_lims,prod=args.prod,Rstar=Rstar)
    tmp = input('Enter to continue.')

# Step in time
t = 0.0
total_iron_1 = 0.0
total_iron_2 = 0.0
next_print = args.reportTime
final_time = 0.0

#total_flux = assemble(flux) * 0.5 * Na*DSid
#print total_flux

while t < T and total_iron_1 < crit_iron:
    t += abs(fdt)

    solver.solve(problem, u.vector())

    if args.plot:
        func = interpolate(u,W)
        func.set_allow_extrapolation(True)
        #plot_mesh_func(mesh,func,idx=2)
        ironPlot.plot_mesh_func_lin(mesh,func,distance,p=ps,nsteps=1000)

    total_flux_1 = assemble(flux1) * Na*DSid
    total_flux_2 = assemble(flux2) * Na*DSid

    if args.cheater:
        total_flux = total_flux_1
        new_total_iron = total_iron_1 + abs(fdt)*total_flux_1
    elif distance == 0.0:
        total_flux = total_flux_1
        new_total_iron = total_iron_1 + abs(fdt)*total_flux
    else:
        total_flux = 0.5*(total_flux_1 + total_flux_2)
        new_total_iron = total_iron_1 + abs(fdt)*total_flux

    u0.assign(u)

    if new_total_iron > crit_iron:
        diff = (crit_iron-total_iron_1)/(new_total_iron-total_iron_1)
        final_time = (t-abs(fdt)) + diff*abs(fdt)
        total_iron_1 = new_total_iron
        if distance > 0.0:
            total_iron_2 += diff*abs(fdt)*total_flux_2
        break
    else:
        total_iron_1 = new_total_iron
        if args.cheater:
            total_iron_2 += abs(fdt)*total_flux_2
        elif distance > 0.0:
            total_iron_2 = new_total_iron

    if fdt < 0.0:
        fdt = fdt * 1.5

        if abs(fdt) > args.maxdt:
            fdt = args.maxdt
            dt.dt = fdt
        else:
            dt.dt = -fdt

        if args.verbose > 1:
            sys.stderr.write("New dt = %g\n" % fdt)

    if t >= next_print:
        next_print += args.reportTime
        if args.output:
          output.write("%12g %12g %12g %12g %12g %12g %12g %12g %12g\n" % \
                       (t, total_flux, total_iron_1, total_iron_2, args.aggk, 
                        args.prod, distance, total_flux_1, total_flux_2))
        else:
          print("%12g %12g %12g %12g %12g %12g %12g %12g %12g" % \
                  (t, total_flux, total_iron_1, total_iron_2, args.aggk, 
                   args.prod, distance, total_flux_1, total_flux_2))

    if args.verbose > 0 and args.reportTime == float("inf"):
        sys.stderr.write("t = %g, iron = %g (%5.2f%%), iron2 = %g         " %
                         (t,total_iron_1,100*total_iron_1/crit_iron,total_iron_2))

        if args.verbose == 1:
            sys.stderr.write("\r")
        else:
            sys.stderr.write("\n")

if args.verbose == 1:
    sys.stderr.write("Done.                                                      \n")

if args.output:
  output.write("%12g %12g %12g %12g %12g %12g %12g %12g %12g\n" % \
               (final_time, 0.0, total_iron_1, total_iron_2, args.aggk, 
                args.prod, distance, 0.0, 0.0))
else:
  print("%12g %12g %12g %12g %12g %12g %12g %12g %12g" % \
          (final_time, 0.0, total_iron_1, total_iron_2, args.aggk, 
         args.prod, distance, 0.0, 0.0))

if args.plot:
    tmp = input('Enter to continue.')

