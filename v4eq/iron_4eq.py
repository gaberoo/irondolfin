
import dolfin
from dolfin import Constant, Expression, Max
from dolfin import dx, assemble
from dolfin import dot, inner, nabla_grad, derivative
from dolfin import FacetNormal, DirichletBC, Measure

import mshr

import iron
import sys

import numpy
import math

zero = Constant(0.0)

class CellBoundary1D(dolfin.SubDomain):
  def __init__(self, rB):
    dolfin.SubDomain.__init__(self)
    self.rB = rB
  def inside(self, x, on_boundary):
    return on_boundary and x < self.rB*1.5

class DomainBoundary1D(dolfin.SubDomain):
  def __init__(self, xmax):
    dolfin.SubDomain.__init__(self)
    self.xmax = xmax
  def inside(self, x, on_boundary):
    return on_boundary and x > self.xmax*.9

def write(out, arr, time=0.0, log10=False):
  if time != None:
    out.write("%.12g " % time)

  for x in arr:
    if log10:
      if x > 0.0:
        out.write("%.12g " % math.log10(x))
      else:
        out.write("-Inf ")
    else:
      out.write("%.12g " % x)

  out.write("\n")
  out.flush()

class SecretorEqs:
  """Reaction-diffusion equations for secretors"""

  def __init__(self, dt, maxTime, k, xmax,
               kX, kL, prod, rho, output=None):
    self.fdt  = dt          # time step
    self.T    = maxTime     # total simulation time
    self.aggk = Constant(k) # iron aggregate size
    self.Fe0  = rho         # background iron

    if self.fdt < 0.0:
      #sys.stderr.write("Increasing dt selected.\n")
      self.dt = Expression("dt", dt=-dt, degree=1)
    else:
      self.dt = Expression("dt", dt=dt, degree=1)

    self.xmax = xmax
    self.eps  = 1e-7
    self.rad  = iron.rB
    self.Fe   = Constant(self.Fe0)

    self.verbose = 0
    self.mesh = None

    self.kX = kX
    self.kL = kL

    self.kappaFX = Expression("kappaFX", kappaFX=kX, degree=1)
    self.kappaEY = Expression("kappaEY", kappaEY=kL, degree=1)

    self.prod = prod

    self.output = output

  def generateMesh(self, nmesh, scale=1.0):
    """Make a 1D mesh"""

    if self.verbose > 0:
      sys.stderr.write("Generating mesh...")

    self.mesh = dolfin.IntervalMesh(nmesh, self.rad, self.xmax)
    self.coords = iron.expand(self.mesh.coordinates(),
                              iron.rB, self.xmax, scale)
    self.mesh.coordinates()[:] = self.coords

    if self.verbose > 0:
      dist = self.coords[1]-self.coords[0]
      sys.stderr.write("done. Minimum distance = %g.\n" % dist)

  def defineFunctionSpace(self):
    self.P1 = dolfin.FiniteElement("CG", dolfin.interval, 1)
    self.element = dolfin.MixedElement([ self.P1, self.P1, self.P1, self.P1 ])
    self.W = dolfin.FunctionSpace(self.mesh, self.element)

  def defineBoundaries(self):
    """Define boundary conditions for the problem"""

    boundaries = dolfin.FacetFunction("size_t", self.mesh)
    boundaries.set_all(0)

    bndry_out  = DomainBoundary1D(self.xmax)
    bndry_cell = CellBoundary1D(iron.rB)

    bndry_cell.mark(boundaries, 1)
    bndry_out.mark(boundaries, 2)

    bc_out_F  = DirichletBC(self.W.sub(0), self.Fe, bndry_out)
    bc_out_X  = DirichletBC(self.W.sub(1), zero,    bndry_out)
    bc_out_Y  = DirichletBC(self.W.sub(2), zero,    bndry_out)
    bc_cell_Y = DirichletBC(self.W.sub(2), zero,    bndry_cell)
    bc_out_E  = DirichletBC(self.W.sub(3), zero,    bndry_out)

    self.bcs = [ bc_out_F, bc_out_X, bc_out_Y, bc_cell_Y, bc_out_E ]

    self.ds = Measure('ds', domain=self.mesh, subdomain_data=boundaries)


  def defineVariationalProblem(self, dim=3, eps=0.0):
    """Define variational problem"""

    # Define trial and test functions
    self.du = dolfin.TrialFunction(self.W)
    self.u  = dolfin.Function(self.W)
    self.u0 = dolfin.Function(self.W)
    self.vF, self.vX, self.vY, self.vE = dolfin.TestFunctions(self.W)

    # Split mixed functions
    self.F,  self.X,  self.Y,  self.E  = dolfin.split(self.u)
    self.F0, self.X0, self.Y0, self.E0 = dolfin.split(self.u0)

    # Create intial conditions and interpolate
    u_init = Expression(("Fe-eps", "eps", "eps", "eps"),
                               Fe=self.Fe0, eps=eps, degree=1)
    self.u.interpolate(u_init)
    self.u0.interpolate(u_init)

    f = Constant(0.0)

    aggk = float(self.aggk)

    self.DFe = Expression("kB*Temp/(6.*pi*h*pow(aggk,1./3.)*rFe)",
                          element=self.P1, kB=iron.kB,
                          Temp=iron.Temp, h=iron.h,
                          aggk=aggk, rFe=iron.rFe)


    FourK = Expression("(aggk > 64) ? 4.0*pow(aggk,-1./3.) : 1.0",
                       aggk=aggk, degree=1)
    fXY = self.kappaFX * self.X * Max(FourK*self.F-self.E, Constant(0.0))
    fEY = self.kappaEY * self.E

    gX = Constant(self.prod)
    n = FacetNormal(self.mesh)

    if dim == 2:
      r2 = Expression("x[0]", degree=1)
      p = Expression("2.0*pi*x[0]", degree=1)
    else:
      r2 = Expression("x[0]*x[0]", degree=1)
      p = Expression("4.0*pi*x[0]*x[0]", degree=1)

    aF = self.dt*r2*self.DFe  * inner(nabla_grad(self.F), nabla_grad(self.vF)) * dx
    aX = self.dt*r2*iron.DSid * inner(nabla_grad(self.X), nabla_grad(self.vX)) * dx
    aY = self.dt*r2*iron.DSid * inner(nabla_grad(self.Y), nabla_grad(self.vY)) * dx
    aE = self.dt*r2*self.DFe  * inner(nabla_grad(self.E), nabla_grad(self.vE)) * dx
    a0 = aF + aX + aY + aE

    # Fk = F/k
    # nS = surface ions per particle = 4 k^(2/3)
    # E0 = nS Fk = 4 k^(-1/3) F
    # when a surface ion is removed, that's 1/k fewer particles

    Lg =   self.dt * r2 * gX  * self.vX * self.ds

    L0 = - self.dt * r2 * fXY * self.vF * dx \
         - self.dt * r2 * fXY * self.vX * dx \
         + self.dt * r2 * fEY * self.vY * dx \
         + self.dt * r2 * (fXY-fEY) * self.vE * dx

    U1 = r2* self.F*self.vF*dx + r2* self.X*self.vX*dx \
            + r2* self.Y*self.vY*dx + r2* self.E*self.vE*dx
    U0 = r2*self.F0*self.vF*dx + r2*self.X0*self.vX*dx \
            + r2*self.Y0*self.vY*dx + r2*self.E0*self.vE*dx

    self.L = (U1-U0) + (a0-L0-Lg)
    self.dL = derivative(self.L, self.u, self.du)

    p = Expression("4.0*pi*x[0]*x[0]", element=self.P1)
    self.flux = -p*dot(nabla_grad(self.Y), n)*self.ds(1)

  def createNonlinearSolver(self):
    """Create nonlinear problem and Newton solver"""

    self.problem = iron.SecretorsEquation(self.dL, self.L, self.bcs)
    self.solver = dolfin.NewtonSolver()
    self.solver.parameters["linear_solver"] = "lu"
    self.solver.parameters["convergence_criterion"] = "incremental"
    self.solver.parameters["relative_tolerance"] = 1e-10

  def run(self, maxdt=float("inf"),
          plot=False, reportTime=float("inf"),
          critIron=float("inf")):
    t = 0.0
    total_iron = 0.0
    aggk = float(self.aggk)
    final_time = 0.0

    if self.output != None:
      numpy.savetxt("%s_coords.txt" % self.output, self.coords[:, 0])
      self.F_out = open("%s_F.txt" % self.output, "w")
      self.X_out = open("%s_X.txt" % self.output, "w")
      self.Y_out = open("%s_Y.txt" % self.output, "w")
      self.E_out = open("%s_E.txt" % self.output, "w")
      self.time_out = open("%s_time.txt" % self.output, "w")
      self.iron_out = open("%s_iron.txt" % self.output, "w")

    next_print = reportTime

    if plot:
      import ironPlot

      maxSid = self.prod*iron.rB**2/(iron.DSid*iron.rB)

      log_coords = [ math.log10(z) for z in self.coords ]
      y = iron.dolf2nump(self.u, self.mesh, dims=4)

      E0 = 4*aggk**(-1./3.)*self.Fe0

      lims = [ (0, 1.1*self.Fe0),
               (0, 1.1*maxSid),
               (0, 1e-9),
               (0, 1.1*E0) ]

      pvals = [ self.prod*iron.rB**2/iron.DSid/r \
                for r in self.coords ]

      p1, p2, p3, p4, time \
              = ironPlot.plot_1D(log_coords, y, lims=lims,
                                 pvals=pvals, Rstar=None)

      raw_input("Press enter to start.")

    while (t < self.T):
      t += abs(self.fdt)

      self.u0.assign(self.u)
      self.solver.solve(self.problem, self.u.vector())

      total_flux = assemble(self.flux) * iron.Na*iron.DSid
      new_total_iron = total_iron + abs(self.fdt)*float(total_flux)

      if new_total_iron > critIron:
        diff = (critIron-total_iron)/(new_total_iron-total_iron)
        final_time = (t-abs(self.fdt)) + diff*abs(self.fdt)
        total_iron = new_total_iron
        break

      else:
        total_iron = new_total_iron
        if self.verbose > 1:
          sys.stderr.write("t = %g, iron = %g (%5.2f%%)         \r" \
                           % (t,total_iron,100*total_iron/critIron))

      if self.fdt < 0.0:
        self.fdt = self.fdt * 1.2
        if abs(self.fdt) > maxdt:
          self.fdt = maxdt
          self.dt.dt = self.fdt
        else:
          self.dt.dt = -self.fdt

      if self.verbose > 1:
        sys.stderr.write("New dt = %g\n" % self.fdt)

      if plot or self.output != None:
        y = iron.dolf2nump(self.u, self.mesh, dims=4)

      if self.output != None:
        write (self.F_out, y[:, 0], time=None)
        write (self.X_out, y[:, 1], time=None)
        write (self.Y_out, y[:, 2], time=None)
        write (self.E_out, y[:, 3], time=None)
        self.time_out.write("%g\n" % t)
        self.iron_out.write("%g %g\n" % (total_flux, total_iron))

      if plot:
        ironPlot.plot_1D(log_coords, y,
                         (p1, p2, p3, p4, time),
                         new_time=t)

      if self.verbose > 0:
        sys.stderr.write("Flux (t=%12f) = %g\r" % (t, total_flux))

      if t >= next_print:
        next_print += reportTime
        print "%g %g %g %g %g %g" % \
                (t, total_flux, total_iron, aggk, self.prod, self.kL)

    if self.output != None:
      self.F_out.close()
      self.X_out.close()
      self.Y_out.close()
      self.E_out.close()
      self.time_out.close()
      self.iron_out.close()

    if self.verbose > 1:
      sys.stderr.write("Done.                                          \n")

    ktime = iron.KeeperTime(aggk)
    if not numpy.isinf(critIron):
        print 0, self.prod, aggk, self.fdt, \
              final_time, self.kL, ktime

    if plot:
      print "Final time = %f" % final_time
      raw_input('Enter to quit.')



