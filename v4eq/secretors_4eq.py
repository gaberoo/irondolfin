#!/usr/bin/env python3

import sys
import signal
import argparse
import math
import numpy

##############################################################################
# Set signal handler

def signal_handler(signal, frame):
  print("Exiting...")
  sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)

##############################################################################
# Write to file

def write(out, arr, time=0.0, log10=False):
  if time != None:
    out.write("%.12g " % time)

  for x in arr:
    if log10:
      if x > 0.0:
        out.write("%.12g " % math.log10(x))
      else:
        out.write("-Inf ")
    else:
      out.write("%.12g " % x)

  out.write("\n")
  out.flush()

##############################################################################
# FEniCS libraries

import mshr

from dolfin import *

import iron

parser = argparse.ArgumentParser(description='Iron diffusion with two cells.')
parser.add_argument('--nmesh', metavar='points', type=int, default=100,
                    help="Number of mesh points")
parser.add_argument('--scale', metavar='value', type=float, default=4.0,
                    help="Mesh stretching parameter")
parser.add_argument('--aggk', metavar='k', type=float, default=1.0,
                    help='Aggregation')
parser.add_argument('--dt', metavar='time', type=float, default=1.0,
                    help='Time step')
parser.add_argument('--maxdt', metavar='time', type=float, default=1000.0,
                    help='Max time step')
parser.add_argument('--reportTime', metavar='time', type=float, 
                    default=float("inf"), help='Reporting time step')
parser.add_argument('--maxTime', metavar='time', type=float, 
                    default=1000.0, help='Max time')
parser.add_argument('--prod', metavar='rate', type=float, default=iron.SidProd,
                    help='Siderophore production rate')
parser.add_argument('--xmax', metavar='pos', type=float, default=0.1,
                    help='Maximum radial distance')
parser.add_argument('--xview', metavar='len', type=float, default=None,
                    help='Viewport size')
parser.add_argument("--output", metavar="file", default=None,
                    help="Output filename")
parser.add_argument("--verbose", metavar="level", type=int, default=0,
                    help="Logging level")
parser.add_argument('--kappaFX', metavar='kappaFX', type=float, default=1e6,
                    help='Binding rate Sid-Fe')
parser.add_argument('--kappaEY', metavar='kappaEY', type=float, default=1e-6,
                    help='Surface dissolution rate')
parser.add_argument("--plot", action="store_true")
parser.add_argument("--dim", metavar='n', type=int, default=3,
                    help="dimensions")
args = parser.parse_args()

##############################################################################
# Numpy/Matplotlib libraries

if args.plot:
  import ironPlot
  import matplotlib as mpl
  mpl.rcParams['toolbar'] = 'None'

##############################################################################
# Form compiler options

parameters["form_compiler"]["optimize"]     = True
parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["representation"] = "uflacs"
set_log_level(LogLevel.WARNING)

##############################################################################
# Some parameters

T    = args.maxTime                # total simulation time
aggk = Constant(args.aggk)  # iron aggregate size
Fe0  = 1e-7

#Rstar, fDFe = iron.R_star(args.prod, aggk, Fe0)
#Rstar, fDFe = iron.R_star_2(args.prod, aggk, args.kappaFX, args.kappaEY)
Rstar = None

xmax = args.xmax
eps  = 1e-7
rad  = iron.rB

##############################################################################
# make the 1D mesh

if args.verbose > 0:
  sys.stderr.write("Generating mesh...")
mesh = IntervalMesh(args.nmesh, iron.rB, xmax)
coords = iron.expand(mesh.coordinates(), iron.rB, xmax, args.scale)
mesh.coordinates()[:] = coords
if args.verbose > 0:
  sys.stderr.write("done. Minimum distance = %g.\n" % \
                   (coords[1]-coords[0]))

##############################################################################
# Define the function space

#V = FunctionSpace(mesh, "CG", 1)  # CG = Lagrange
P1 = FiniteElement("CG", interval, 1)
element = MixedElement([ P1, P1, P1, P1 ])
W = FunctionSpace(mesh, element)

##############################################################################
# Define boundaries

Fe = Constant(Fe0)
zero = Constant(0.0)

boundaries = MeshFunction("size_t", mesh, 0)
boundaries.set_all(0)

bndry_cell = CompiledSubDomain("on_boundary && x[0] == rB", rB=iron.rB)
bndry_out  = CompiledSubDomain("on_boundary && x[0] == xmax", xmax=xmax)

bndry_cell.mark(boundaries, 1)
bndry_out.mark(boundaries, 2)

bc_out_F  = DirichletBC(W.sub(0), Fe,    bndry_out)
bc_out_X  = DirichletBC(W.sub(1), zero,  bndry_out)
bc_out_Y  = DirichletBC(W.sub(2), zero,  bndry_out)
bc_cell_Y = DirichletBC(W.sub(2), zero,  bndry_cell)
bc_out_E  = DirichletBC(W.sub(3), zero,  bndry_out)

bcs = [ bc_out_F, bc_out_X, bc_out_Y, bc_cell_Y, bc_out_E ]

##############################################################################
# Setup measure for the boundaries (needed to calculate flux)

ds = Measure('ds', domain=mesh, subdomain_data=boundaries)

##############################################################################
# Define variational problem

# Define trial and test functions
du = TrialFunction(W)
u  = Function(W)
vF, vX, vY, vE = TestFunctions(W)

# Define functions

# Split mixed functions
F,  X,  Y,  E  = split(u)

# Create intial conditions and interpolate
u_init = Expression(("Fe-eps", "eps", "eps", "eps"),
                           Fe=Fe0, eps=Fe0/2.0, degree=1)
u.interpolate(u_init)

kappaFX = Expression("kappaFX", kappaFX=args.kappaFX, degree=1)
kappaEY = Expression("kappaEY", kappaEY=args.kappaEY, degree=1)

gX = Constant(args.prod)
DFe = Expression("kB*Temp/(6.*pi*h*pow(aggk,1./3.)*rFe)",
                        element=P1, kB=iron.kB, Temp=iron.Temp,
                        h=iron.h, aggk=aggk, rFe=iron.rFe)

FourK = Expression("(aggk > 64) ? 4.0*pow(aggk,-1./3.) : 1.0",
                          aggk=aggk, degree=1)
fXY = kappaFX*X*iron.Max(FourK*F - E, Constant(0.0))
fEY = kappaEY*E

n = FacetNormal(mesh)

if args.dim == 2:
  r2 = Expression("x[0]", degree=1)
  p = Expression("2.0*pi*x[0]", degree=1)
else:
  r2 = Expression("x[0]*x[0]", degree=1)
  p = Expression("4.0*pi*x[0]*x[0]", degree=1)

aF = r2*DFe       * inner(nabla_grad(F), nabla_grad(vF)) * dx
aX = r2*iron.DSid * inner(nabla_grad(X), nabla_grad(vX)) * dx
aY = r2*iron.DSid * inner(nabla_grad(Y), nabla_grad(vY)) * dx
aE = r2*DFe       * inner(nabla_grad(E), nabla_grad(vE)) * dx
a0 = aF + aX + aY + aE

Lg = r2*gX*vX*ds
L0 = - r2*fXY*vF*dx \
     - r2*fXY*vX*dx \
     + r2*fEY*vY*dx \
     + r2*(fXY-fEY)*vE*dx

L = a0 - L0 - Lg

##############################################################################
# Create nonlinear problem and Newton solver

solve(L == 0.0, u, bcs=bcs)

n = FacetNormal(mesh)
p = Expression("4.0*pi*x[0]*x[0]", element=P1)
flux = -p*dot(nabla_grad(Y), n)*ds(1)

if args.output != None:
  #u_out = File(args.output)
  numpy.savetxt("%s_coords.txt" % args.output, coords[:, 0])
  F_out = open("%s_F.txt" % args.output, "w")
  X_out = open("%s_X.txt" % args.output, "w")
  Y_out = open("%s_Y.txt" % args.output, "w")
  E_out = open("%s_E.txt" % args.output, "w")
  time_out = open("%s_time.txt" % args.output, "w")
  iron_out = open("%s_iron.txt" % args.output, "w")

if args.plot:
  maxSid = args.prod*iron.rB**2/(iron.DSid*iron.rB)
  log_coords = [ math.log10(z) for z in coords ]
  y = iron.dolf2nump(u, mesh, dims=4)
  E0 = 4*args.aggk**(-1./3.)*Fe0
  lims = [ (0, 1.1*Fe0),
           (0, 1.1*maxSid),
           (0, 1e-9),
           (0, 1.1*E0) ]
  pvals = [ args.prod*iron.rB**2/iron.DSid/r for r in coords ]
  ironPlot.plot_1D(log_coords, y, lims=lims, pvals=pvals, Rstar=Rstar)
  #ax4.axhline(E0, color="grey")
  #Xr = [ args.prod*iron.rB**2/(iron.DSid*rr) for rr in coords ]
  #Estar = [ 1.0/(1.0+args.kappaEY/(args.kappaFX*a)) for a in Xr ]
  #print Estar
  #ax4.plot(log_coords, Estar, color="#cccccc", linewidth=2)

t = 0.0
next_print = args.reportTime
total_iron = 0.0

if args.plot:
  raw_input('Enter to quit.')

