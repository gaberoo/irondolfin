#!/bin/bash

control_c() {
  echo -en "\n*** Ouch! Exiting ***\n"
  exit $?
}
trap control_c SIGINT

prod="1e-9"
for aggk in $( cat aggk.txt )
do
  for w in $( cat weathering.txt )
  do
    ./single_uptake_time_4eq.py --dt -0.01 --aggk $aggk \
      --maxdt 1e6 --nmesh 400 --xmax 10.0 --prod $prod \
      --kappaEY $w
  done
done

