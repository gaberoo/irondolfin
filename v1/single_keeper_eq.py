#!/usr/bin/env python3

import os
import sys
import signal
import argparse
import math
import numpy

os.environ['CC'] = 'gcc-8'
os.environ['CXX'] = 'g++-8'

##############################################################################
# Set signal handler

def signal_handler(signal, frame):
  print("Exiting...")
  sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)

##############################################################################
# FEniCS libraries

import mshr
from dolfin import *

import iron
args = iron.parser.parse_args()

##############################################################################
# Numpy/Matplotlib libraries

if args.plot:
  import ironPlot
  import matplotlib as mpl
  import matplotlib.pyplot as plt
  mpl.rcParams['toolbar'] = 'None'

##############################################################################
# Form compiler options

parameters["form_compiler"]["optimize"]     = True
parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["representation"] = "uflacs"
set_log_level(LogLevel.WARNING) # 30 = warning, 

##############################################################################
# Some parameters

zero = Constant(0.0)

fdt  = args.dt
T    = args.maxTime         # total simulation time
aggk = Constant(args.aggk)  # iron aggregate size
Fe0  = 1e-6

fF = 1e1
gF = 1e2

if fdt < 0.0:
  sys.stderr.write("Increasing dt selected.\n")
  dt = Expression("dt", dt=-fdt, degree=1)
else:
  dt = Expression("dt", dt=fdt, degree=1)

xmax = args.xmax
eps  = 1e-7
rad  = iron.rB

##############################################################################
# make the 1D mesh

if args.verbose > 0:
  sys.stderr.write("Generating mesh...")
mesh = IntervalMesh(args.nmesh, iron.rB, xmax)
coords = iron.expand(mesh.coordinates(), iron.rB, xmax, args.scale)
mesh.coordinates()[:] = coords
if args.verbose > 0:
  sys.stderr.write("done. Minimum distance = %g.\n" % \
                   (coords[1]-coords[0]))

#mesh = Mesh("meshes/linear.xml.gz")
#coords = mesh.coordinates()

##############################################################################
# Define the function space

element = FiniteElement("Lagrange", interval, 1)
W = FunctionSpace(mesh, element)

##############################################################################
# Define boundaries

boundaries = MeshFunction("size_t", mesh, 0)
boundaries.set_all(0)

bndry_cell = CompiledSubDomain("on_boundary && x[0] == rB", rB=iron.rB)
bndry_out  = CompiledSubDomain("on_boundary && x[0] == xmax", xmax=xmax)

bndry_cell.mark(boundaries, 1)
bndry_out.mark(boundaries, 2)

bc_out  = DirichletBC(W, Constant(Fe0), bndry_out)
bc_cell = DirichletBC(W, Constant(Fe0), bndry_cell)

bcs = [ bc_out, bc_cell ]

##############################################################################
# Setup measure for the boundaries (needed to calculate flux)

ds = Measure('ds', domain=mesh, subdomain_data=boundaries)

##############################################################################
# Define variational problem

# Define trial and test functions
dF = TrialFunction(W)
F  = Function(W)
vF = TestFunction(W)

# Define functions

# Create intial conditions and interpolate
u_init = Expression(("Fe0"), Fe0=Fe0, degree=1)
F.interpolate(u_init)

f = Constant(0.0)
DFe = Constant(iron.Diff(iron.rFe))
DFk = Expression("kB*Temp/(6.*pi*h*pow(aggk,1./3.)*rFe)",
                 element=element, kB=iron.kB, Temp=iron.Temp,
                 h=iron.h, aggk=aggk, rFe=iron.rFe)

n = FacetNormal(mesh)

if args.dim == 2:
  r2 = Expression("x[0]", degree=1)
  p = Expression("2.0*pi*x[0]", degree=1)
else:
  r2 = Expression("x[0]*x[0]", degree=1)
  p = Expression("4.0*pi*x[0]*x[0]", degree=1)

aF  = dt*r2*DFe*inner(nabla_grad(F),nabla_grad(vF))*dx

KH = Constant(Fe0*1e-8)
maxFlux = 10000.0
phiMax = Constant(maxFlux/iron.Na/(4*math.pi*iron.rB**2))
if args.verbose: print("Maximum flux = %g" % maxFlux)

Lu = - dt*r2*phiMax*(F/(KH+F))*vF*ds(1)

U1 = r2*F*vF*dx
U0 = r2*F0*vF*dx

L = (U1-U0) + (aF-Lu)
dL = derivative(L, F, dF)

p = Expression("4.0*pi*x[0]*x[0]", element=element)
flux = -p*dot(nabla_grad(F),n)*ds(1)

##############################################################################
# Create nonlinear problem and Newton solver

problem = iron.SecretorsEquation(dL, L, bcs)
solver = NewtonSolver()
solver.parameters["linear_solver"] = "lu"
solver.parameters["convergence_criterion"] = "incremental"
solver.parameters["relative_tolerance"] = 1e-10

t = 0.0
total_iron = 0.0

if args.output != None:
  numpy.savetxt("%s_coords.txt" % args.output, coords[:, 0])
  F_out = open("%s_F.txt" % args.output, "w")
  time_out = open("%s_time.txt" % args.output, "w")
  iron_out = open("%s_iron.txt" % args.output, "w")

t = 0.0
next_print = args.reportTime
total_iron = 0.0

if args.plot:
  log_coords = [ math.log10(z) for z in coords ]
  y = iron.dolf2nump(F, mesh, dims=1)
  fig, ax1 = plt.subplots()
  ax1.set_ylim([0, 1.1e-6])
  p1, = ax1.plot(log_coords, y[:, 0], color="#4daf4a", linewidth=2.0)
  plt.show(block=False)
  plt.pause(0.01)

while (t < T):
  t += abs(fdt)

  F0.assign(F)
  sol = solver.solve(problem, F.vector())

  total_flux = assemble(flux) * iron.Na*float(DFe)
  total_iron += abs(fdt)*float(total_flux)

  if fdt < 0.0:
    fdt = fdt * 1.2
    if abs(fdt) > args.maxdt:
      fdt = args.maxdt
      dt.dt = fdt
    else:
      dt.dt = -fdt

  if args.verbose > 1:
    sys.stderr.write("New dt = %g\n" % fdt)

  if args.plot or args.output != None:
    y = iron.dolf2nump(F, mesh, dims=1)

  if args.plot:
    p1.set_data(log_coords, y[:, 0])
    plt.draw()
    plt.pause(0.01)

  if args.output != None:
    iron.write(F_out, y[:, 1], time=None)
    time_out.write("%g\n" % t)
    iron_out.write("%g %g\n" % (total_flux, total_iron))

  if args.verbose > 0:
    sys.stderr.write("Flux (t=%12f) = %g\r" % (t, total_flux))

  if t >= next_print:
    next_print += args.reportTime
    print("%g %g %g %g %g %g" % (
          t, total_flux, total_iron, args.aggk, 
          assemble(flux)*float(DFe), 
          iron.KeeperFlux(float(aggk),Fe0)))

if args.output != None:
  F_out.close()
  time_out.close()
  iron_out.close()

